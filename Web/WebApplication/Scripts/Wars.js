var WarTS = (function () {
    function WarTS() {
    }
    WarTS.prototype.afficher = function () {
        var res = '<div class="war" id="' + this.WarId + '">' + this.displayFights() + '</div>';
        return res;
    };
    WarTS.prototype.displayFights = function () {
        var res;
        $.each(this.Fights, function (index, elt) {
            res += elt.afficher();
        });
        return res;
    };
    return WarTS;
}());
//# sourceMappingURL=Wars.js.map