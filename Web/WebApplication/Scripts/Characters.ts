﻿$(document).ready(function () {

    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/character",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            let list: CharacterTS[] = [];
            $.each(data, function (index, elt) {
                let elem: CharacterTS = new CharacterTS();
                elem.FirstName = elt.FirstName;
                elem.LastName = elt.LastName;
                elem.Bravoury = elt.Bravoury;
                elem.Crazyness = elt.Crazyness;
                elem.Pv = elt.Pv;
                elem.Relationships = elt.Relationships;
                elem.CharacterType = elt.CharacterType;
                elem.Id = elt.Id;
                elem.house = elt.house;
                list.push(elem);
            });

            list.forEach(function (elt: CharacterTS) {
                $('#div_list_character').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    })
});

class CharacterTS {
    public Bravoury: number;
    public Crazyness: number;
    public FirstName: string;
    public LastName: string;
    public Pv: number;
    public Relationships: string;
    public CharacterType: number;
    public house: number;
    public Id: number;

    constructor() { }

    public afficher(): string {
        let res: string= '<div><h4>' + this.FirstName +' '+this.LastName+ '</h4></div>';
        return res;
    }
}