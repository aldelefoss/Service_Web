$(document).ready(function () {
    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/territory",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var list = [];
            $.each(data, function (index, elt) {
                var house = new TerritoryTS();
                house.Type = elt.type;
                house.Owner = new HouseTS();
                if (elt.Owner) {
                    house.Owner.Name = elt.Owner.Name;
                    house.Owner.NumberOfUnits = elt.Owner.NumberOfUnits;
                    house.Owner.Id = elt.Owner.Id;
                    console.log(elt.Owner);
                }
                house.Id = elt.Id;
                list.push(house);
            });
            list.forEach(function (elt) {
                $('#div_list_territory').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    });
});
var TerritoryTS = (function () {
    function TerritoryTS() {
    }
    TerritoryTS.prototype.afficher = function () {
        var res = '<div><h4 id="territory">' + this.Type + '</h4><p><span>Id :</span>' + this.Id + '</p><p><span>Id Owner :</span>' + this.Owner.Name + '</p></div>';
        return res;
    };
    return TerritoryTS;
}());
//# sourceMappingURL=Territories.js.map