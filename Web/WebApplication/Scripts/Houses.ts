﻿$(document).ready(function () {

    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/house",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            let list: HouseTS[] = [];
            $.each(data, function (index, elt) {
                let house: HouseTS = new HouseTS();
                house.Name = elt.Name;
                house.NumberOfUnits = elt.NumberOfUnits;
                house.Id = elt.Id;
                list.push(house);
            });

            list.forEach(function (elt: HouseTS) {
                $('#div_list_house').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    })
});

class HouseTS
{
    public Name: string;
    public NumberOfUnits: string;
    public Id: number;

    constructor() { }

    public afficher(): JQuery{
        let myThis: HouseTS = this;
        let res: JQuery = $('<div id="' + this.Id + '"><h4>' + this.Name + '</h4><p>' + this.NumberOfUnits + '</p><span><i class="fas fa-trash-alt"></i></span></div><hr>');
        res[0].onclick = function (e) { myThis.showInfo(); };
        return res;
    }

    public showInfo(): void {

        window.location.href = "/House/Details/"+this.Id;

    }
}