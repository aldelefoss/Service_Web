﻿$(document).ready(function () {

    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/fight",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            let list: CombatTS[] = [];
            $.each(data, function (index, elt) {
                let fight: CombatTS = new CombatTS();
                fight.houseChallenging = elt.houseChallenging;
                fight.houseChallenged = elt.houseChallenged;
                fight.houseWinner = elt.houseWinner;
                fight.fightId = elt.Id;
                fight.warId = elt.warId;

                list.push(fight);
            });

            list.forEach(function (elt: CombatTS) {
                $('#div_list_fight').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    });
});

class CombatTS {
    public houseChallenging: HouseTS;
    public houseChallenged: HouseTS;
    public houseWinner: HouseTS;
    public fightId: number;
    public warId: number;

    constructor() { }

    public afficher(): JQuery {
        let myThis: CombatTS = this;
        let res: JQuery = $('<div class="fight" id="div_fight_'+this.fightId+'">' + this.displayHouse(this.houseChallenging) + ' challenge ' + this.displayHouse(this.houseChallenged) + ' gagné par ' + this.displayHouse(this.houseWinner) + '</div><a class="delete" id="a_del_'+this.fightId+'"> Delete </a><hr/>');
        res[1].onclick = function (e) { myThis.delete(); };
        return res;
    }

    public delete() {
        let id = this.fightId;
        $.ajax({
            type: "DELETE",
            url: "http://localhost:15545/api/fight/"+id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#div_fight_" + id).remove();
                $("#a_del_" + id).remove();
            },
            error: function (error) {
                console.log("Error :");
                console.log(error);
            }
        });     
    }

    public displayHouse(house : HouseTS) :string {
        return (house) ? '<div id="' + house.Id + '"><h4 id="combat">' + house.Name + '</h4><p>' + house.NumberOfUnits + '</p></div>' : "<p><em>EN COURS</em></p>";
    }

}