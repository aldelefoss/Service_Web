$(document).ready(function () {
    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/house",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var list = [];
            $.each(data, function (index, elt) {
                var house = new HouseTS();
                house.Name = elt.Name;
                house.NumberOfUnits = elt.NumberOfUnits;
                house.Id = elt.Id;
                list.push(house);
            });
            list.forEach(function (elt) {
                $('#div_list_house').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    });
});
var HouseTS = (function () {
    function HouseTS() {
    }
    HouseTS.prototype.afficher = function () {
        var myThis = this;
        var res = $('<div id="' + this.Id + '"><h4>' + this.Name + '</h4><p>' + this.NumberOfUnits + '</p><span><i class="fas fa-trash-alt"></i></span></div><hr>');
        res[0].onclick = function (e) { myThis.showInfo(); };
        return res;
    };
    HouseTS.prototype.showInfo = function () {
        window.location.href = "/House/Details/" + this.Id;
    };
    return HouseTS;
}());
//# sourceMappingURL=Houses.js.map