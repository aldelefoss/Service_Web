$(document).ready(function () {
    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/character",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var list = [];
            $.each(data, function (index, elt) {
                var elem = new CharacterTS();
                elem.FirstName = elt.FirstName;
                elem.LastName = elt.LastName;
                elem.Bravoury = elt.Bravoury;
                elem.Crazyness = elt.Crazyness;
                elem.Pv = elt.Pv;
                elem.Relationships = elt.Relationships;
                elem.CharacterType = elt.CharacterType;
                elem.Id = elt.Id;
                elem.house = elt.house;
                list.push(elem);
            });
            list.forEach(function (elt) {
                $('#div_list_character').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    });
});
var CharacterTS = (function () {
    function CharacterTS() {
    }
    CharacterTS.prototype.afficher = function () {
        var res = '<div><h4>' + this.FirstName + ' ' + this.LastName + '</h4></div>';
        return res;
    };
    return CharacterTS;
}());
//# sourceMappingURL=Characters.js.map