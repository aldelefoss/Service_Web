$(document).ready(function () {
    var Id = $('h2').attr('id');
    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/house/" + Id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var house = new HouseDetailsTS();
            house.Name = data.Name;
            house.NumberOfUnits = data.NumberOfUnits;
            house.Id = data.Id;
            $.ajax({
                type: "GET",
                data: {},
                url: "http://localhost:15545/api/house/" + Id + "/characters",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    house.Characters = [];
                    $.each(data, function (index, elt) {
                        var elem = new CharacterTS();
                        elem.FirstName = elt.FirstName;
                        elem.LastName = elt.LastName;
                        elem.Bravoury = elt.Bravoury;
                        elem.Crazyness = elt.Crazyness;
                        elem.Pv = elt.Pv;
                        elem.Relationships = elt.Relationships;
                        elem.CharacterType = elt.CharacterType;
                        elem.Id = elt.Id;
                        elem.house = elt.house;
                        (house.Characters).push(elem);
                    });
                    $.ajax({
                        type: "GET",
                        data: {},
                        url: "http://localhost:15545/api/house/" + Id + "/fights",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            house.Fights = [];
                            $.each(data.Submited, function (index, elt) {
                                var fight = new CombatTS();
                                fight.houseChallenging = elt.houseChallenging;
                                fight.houseChallenged = elt.houseChallenged;
                                fight.houseWinner = elt.houseWinner;
                                fight.fightId = elt.Id;
                                fight.warId = elt.warId;
                                (house.Fights).push(fight);
                            });
                            $.each(data.Initiated, function (index, elt) {
                                var fight = new CombatTS();
                                fight.houseChallenging = elt.houseChallenging;
                                fight.houseChallenged = elt.houseChallenged;
                                fight.houseWinner = elt.houseWinner;
                                fight.fightId = elt.Id;
                                fight.warId = elt.warId;
                                (house.Fights).push(fight);
                            });
                            $.ajax({
                                type: "GET",
                                data: {},
                                url: "http://localhost:15545/api/house/" + Id + "/territories",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    house.Territories = [];
                                    $.each(data, function (index, elt) {
                                        var h = new TerritoryTS();
                                        h.Type = elt.type;
                                        h.Owner = elt.Owner;
                                        h.Id = elt.Id;
                                        (house.Territories).push(h);
                                    });
                                    $.ajax({
                                        type: "GET",
                                        data: {},
                                        url: "http://localhost:15545/api/house/" + Id + "/wars",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {
                                            house.Wars = [];
                                            $.each(data, function (index, elt) {
                                                var war = new WarTS();
                                                war.WarId = index;
                                                war.Fights = [];
                                                $.each(elt.Fights, function (index2, elt2) {
                                                    var fight = new CombatTS();
                                                    fight.houseChallenged = elt2.houseChallenged;
                                                    fight.houseChallenging = elt2.houseChallenging;
                                                    fight.houseWinner = elt2.houseWinner;
                                                    (war.Fights).push(fight);
                                                });
                                                (house.Wars).push(war);
                                            });
                                            console.log(house);
                                            $("#content").append(house.afficher());
                                        },
                                        error: function (error) {
                                            console.log("Error wars:");
                                            console.log(error);
                                        }
                                    });
                                },
                                error: function (error) {
                                    console.log("Error territories:");
                                    console.log(error);
                                }
                            });
                        },
                        error: function (error) {
                            console.log("Error fights:");
                            console.log(error);
                        }
                    });
                },
                error: function (error) {
                    console.log("Error characters:");
                    console.log(error);
                }
            });
        },
        error: function (error) {
            console.log("Error primary data:");
            console.log(error);
        }
    });
});
var HouseDetailsTS = (function () {
    function HouseDetailsTS() {
    }
    HouseDetailsTS.prototype.afficher = function () {
        var res = $('<div id="' + this.Id + '"><h4 id="test">' + this.Name + '</h4><p>' + this.NumberOfUnits + '</p>' + (this.displayCharacters()).replace(/undefined/gi, "") + '<p><h3>Fights</h3>' + (this.displayFights()).replace(/undefined/gi, "") + '</p><p><h3>Territory</h3>' + (this.displayTerritories()).replace(/undefined/gi, "") + '</p><p><h3>Wars</h3>' + (this.displayWars()).replace(/undefined/gi, "") + '</p>');
        return res;
    };
    HouseDetailsTS.prototype.showInfo = function () {
        window.location.href = "/House/Details/" + this.Id;
    };
    HouseDetailsTS.prototype.displayCharacters = function () {
        var res;
        $.each(this.Characters, function (index, elt) {
            res += elt.afficher();
        });
        return res;
    };
    HouseDetailsTS.prototype.displayFights = function () {
        var res;
        $.each(this.Fights, function (index, elt) {
            res += elt.afficher();
        });
        return res;
    };
    HouseDetailsTS.prototype.displayTerritories = function () {
        var res;
        $.each(this.Territories, function (index, elt) {
            res += elt.afficher();
        });
        return res;
    };
    HouseDetailsTS.prototype.displayWars = function () {
        var res;
        $.each(this.Wars, function (index, elt) {
            res += elt.afficher();
        });
        return res;
    };
    return HouseDetailsTS;
}());
//# sourceMappingURL=Details.js.map