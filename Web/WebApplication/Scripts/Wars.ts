﻿class WarTS {
    public Fights: CombatTS[];
    public WarId: number;


    constructor() { }


    public afficher() {
        let res: string = '<div class="war" id="'+this.WarId+'">' + this.displayFights() +'</div>';
        return res;
    }

    public displayFights() :string{
        let res: string;
        $.each(this.Fights, function (index, elt) {
            res += elt.afficher();
        });

        return res;
    }
}