$(document).ready(function () {
    $.ajax({
        type: "GET",
        data: {},
        url: "http://localhost:15545/api/fight",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var list = [];
            $.each(data, function (index, elt) {
                var fight = new CombatTS();
                fight.houseChallenging = elt.houseChallenging;
                fight.houseChallenged = elt.houseChallenged;
                fight.houseWinner = elt.houseWinner;
                fight.fightId = elt.Id;
                fight.warId = elt.warId;
                list.push(fight);
            });
            list.forEach(function (elt) {
                $('#div_list_fight').append(elt.afficher());
            });
        },
        error: function (error) {
            console.log("Error :");
            console.log(error);
        }
    });
});
var CombatTS = (function () {
    function CombatTS() {
    }
    CombatTS.prototype.afficher = function () {
        var myThis = this;
        var res = $('<div class="fight" id="div_fight_' + this.fightId + '">' + this.displayHouse(this.houseChallenging) + ' challenge ' + this.displayHouse(this.houseChallenged) + ' gagné par ' + this.displayHouse(this.houseWinner) + '</div><a class="delete" id="a_del_' + this.fightId + '"> Delete </a><hr/>');
        res[1].onclick = function (e) { myThis.delete(); };
        return res;
    };
    CombatTS.prototype.delete = function () {
        var id = this.fightId;
        $.ajax({
            type: "DELETE",
            url: "http://localhost:15545/api/fight/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#div_fight_" + id).remove();
                $("#a_del_" + id).remove();
            },
            error: function (error) {
                console.log("Error :");
                console.log(error);
            }
        });
    };
    CombatTS.prototype.displayHouse = function (house) {
        return (house) ? '<div id="' + house.Id + '"><h4 id="combat">' + house.Name + '</h4><p>' + house.NumberOfUnits + '</p></div>' : "<p><em>EN COURS</em></p>";
    };
    return CombatTS;
}());
//# sourceMappingURL=Combats.js.map