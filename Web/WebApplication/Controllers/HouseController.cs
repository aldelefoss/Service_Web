﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HouseController : Controller
    {
        // GET: House
        public async Task<ActionResult> Index()
        {
            return View();
        }

        public async Task<ActionResult> Details(int id)
        {
            ViewBag.Id = id;
              return View();
        }

    }
}
