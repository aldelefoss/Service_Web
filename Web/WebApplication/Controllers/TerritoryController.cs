﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class TerritoryController : Controller
    {
        // GET: Territory
        public async Task<ActionResult> Index()
        {
            /*using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15545");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                 );
                HttpResponseMessage response = await client.GetAsync("api/territory");

                if (response.IsSuccessStatusCode)
                {
                    string tmp = await response.Content.ReadAsStringAsync();
                    TerritoriesModel land = new TerritoriesModel(JsonConvert.DeserializeObject<List<TerritoryModel>>(tmp));
                    return View(land);
                }

            }*/
            return View();
        }

        // GET: Territory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Territory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Territory/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Territory/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Territory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Territory/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Territory/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
