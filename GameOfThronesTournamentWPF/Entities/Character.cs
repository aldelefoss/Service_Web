﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    [Serializable]
    public class Character
    {
        public int Id { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Pv { get; set; }


        public string CharacterType { get; set; }

        public int house { get; set; }
        public Character()
        {

        }
        public Character(int id, int bravoury, int crazyness, string firstName, string lastName, int pv, int houseId)
        {
            Id = id;
            this.Bravoury = bravoury;
            this.Crazyness = crazyness;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Pv = pv;
            this.house = houseId;
        }

    }
}
