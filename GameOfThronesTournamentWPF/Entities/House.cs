﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    [Serializable]
    public class House
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfUnits { get; set; }
        public House()
        {

        }
        public House(int id, string n, int number)
        {
            Id = id;
            Name = n;
            NumberOfUnits = number;
        }
    }
}
