﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Fight
    {
        public int Id { get; set; }
        public House houseChallenging { get; set; }
        public House houseChallenged { get; set; }
        public House houseWinner { get; set; }

        public Fight(int id, House Challenging, House Challenged, House Winner)
        {
            Id = id;
            houseChallenged = Challenged;
            houseChallenging = Challenging;
            houseWinner = Winner;
        }
    }
}
