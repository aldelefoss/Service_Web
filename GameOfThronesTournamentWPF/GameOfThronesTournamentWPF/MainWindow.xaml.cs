﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace GameOfThronesTournamentWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void getHouses(object sender, RoutedEventArgs e)
        {
            Window houses = new Houses();
            houses.Show();
        }

        private void getCharacters(object sender, RoutedEventArgs e)
        {
            Window houses = new Characters();
            houses.Show();
        }

        private void getFights(object sender, RoutedEventArgs e)
        {
            Window houses = new Fights();
            houses.Show();
        }

        private void exportHouses(object sender, RoutedEventArgs e)
        {
            getHouses();
        }


        async void getHouses()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15545");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await client.GetAsync("api/house");
                if (res.IsSuccessStatusCode)
                {
                    string tmp = await res.Content.ReadAsStringAsync();
                    Console.WriteLine(tmp);
                    List<House> houses = JsonConvert.DeserializeObject<List<House>>(tmp);
                    Console.WriteLine(tmp);

                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();



                    // Set filter for file extension and default file extension 
                    dlg.DefaultExt = ".xml";
                    dlg.Filter = "xml Files (*.xml)|*.xml";

                    Nullable<bool> result = dlg.ShowDialog();


                    if (result == true)
                    {
                        string filename = dlg.FileName;
                        Char delimiter = ',';
                        if (filename.Split(delimiter).Count() == 1)
                        {
                            filename = filename + ".xml"; 
                        }
                        XmlSerializer serialiser = new XmlSerializer(typeof(List<House>));
                        TextWriter FileStream = new StreamWriter(filename);
                        serialiser.Serialize(FileStream, houses);
                        FileStream.Close();
                        Console.WriteLine(filename);
                    }

                }


            }
        }
    }
}
