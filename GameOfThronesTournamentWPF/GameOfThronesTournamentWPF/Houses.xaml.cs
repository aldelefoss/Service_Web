﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF
{
    /// <summary>
    /// Logique d'interaction pour Houses.xaml
    /// </summary>
    public partial class Houses : Window
    {
        List<House> houses;
        DataTable table;
        public Houses()
        {
            InitializeComponent();
            houses = new List<House>();
            getHouses();
            table = new DataTable();
        }

        async void getHouses()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15545");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await client.GetAsync("api/house");
                if (res.IsSuccessStatusCode)
                {
                    string tmp = await res.Content.ReadAsStringAsync();
                    Console.WriteLine(tmp);
                    houses = JsonConvert.DeserializeObject<List<House>>(tmp);
                    Console.WriteLine(tmp);
                    dg.ItemsSource = houses;

                }


            }
        }

    }
}
