﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF
{
    /// <summary>
    /// Logique d'interaction pour Fights.xaml
    /// </summary>
    public partial class Fights : Window
    {
        List<Fight> fights;
        public Fights()
        {
            InitializeComponent();
            fights = new List<Fight>();
            getHouses();
        }
        async void getHouses()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15545");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await client.GetAsync("api/fight");
                if (res.IsSuccessStatusCode)
                {
                    string tmp = await res.Content.ReadAsStringAsync();
                    Console.WriteLine(tmp);
                    fights = JsonConvert.DeserializeObject<List<Fight>>(tmp);
                    dg.ItemsSource = fights;
                }


            }
        }
    }

}
