﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF
{
    /// <summary>
    /// Logique d'interaction pour Characters.xaml
    /// </summary>
    public partial class Characters : Window
    {
        List<Character> characters;
        public Characters()
        {
            InitializeComponent();
            characters = new List<Character>();
            getCharacters();
        }
        async void getCharacters()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15545");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await client.GetAsync("api/character");
                if (res.IsSuccessStatusCode)
                {
                    string tmp = await res.Content.ReadAsStringAsync();
                    Console.WriteLine(tmp);
                    characters = JsonConvert.DeserializeObject<List<Character>>(tmp);
                    dg.ItemsSource = characters;

                }
            }
        }
    }
}
