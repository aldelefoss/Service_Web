﻿using System;
using EntitiesLayer;
using DataAccessLayer;
using System.Collections.Generic;
using System.Data;
using RestAPI.Models;

namespace BuisinessLayer
{
    public class ThronesTournamentManager
    {
        public DalManager dalManager;

        public ThronesTournamentManager()
        {
            dalManager = DalManager.Instance;
        }
        public List<Character> getCharacters()
        {
            DataTable datatable = dalManager.database.getAllCharacters();
            List<Character> objs = new List<Character>();

            foreach (DataRow row in datatable.Rows)
            {

                int houseId = -1;
                try
                {
                    houseId = int.Parse(row["HouseId"].ToString());
                }
                catch (Exception e)
                {

                    houseId = -1;
                }

                Character ch = new Character(
                        int.Parse(row["Id"].ToString()),
                        int.Parse(row["Bravoury"].ToString()),
                        int.Parse(row["Crazyness"].ToString()),
                        row["FirstName"].ToString(),
                        row["LastName"].ToString(),
                        int.Parse(row["Pv"].ToString()),
                        (CharacterTypeEnum)int.Parse(row["CharacterType"].ToString()),
                        houseId);
                objs.Add(ch);
            }
            return objs;
        }


        public int[] getScore(House h1, House h2, Territory t)
        {
            h1.Housers = getCharacters(h1);
            h2.Housers = getCharacters(h2);
            int score1, score2;
            if (h2.Housers.Count == 0) score2 = 0;
            if (h1.Housers.Count == 0) score1 = 0;
            if (h2.Housers.Count == 0 && h1.Housers.Count == 0) return new int[]{ 0, 0};

            // Prendre en compte le nbr d'unité
            score1 = h1.NumberOfUnits + h1.Housers.Count * 10;
            score2 = h2.NumberOfUnits + h2.Housers.Count * 10;


            // Prendre en compte le terrain
            if (getTerritories(h1).Find(x => x.Id == t.Id) != null)
            {
                score1 += 100;

            }            
            if (getTerritories(h2).Find(x => x.Id == t.Id) != null)
            {
                score2 += 100;

            }



            // Prendre en compte les relation entre les personnages
            foreach (Character c in h1.Housers)
            {

                foreach (Relationship r in getRelationships(c))
                {

                    // personnage d'autre house
                    Character character = h2.Housers.Find(x => x.Id == r.Character.Id);
                    if (character != null)
                    {
                        if (r.Type == RelationshipEnum.FRIENDSHIP || r.Type == RelationshipEnum.LOVE) score1 -= 50;
                        else score1 += 15;

                    }

                    // personnage du meme house 
                    character = h1.Housers.Find(x => x.Id == r.Character.Id);
                    if (character != null)
                    {
                        if (r.Type == RelationshipEnum.FRIENDSHIP || r.Type == RelationshipEnum.LOVE) score1 += 15;
                        else score1 -= 10;

                    }
                }

            }

            foreach (Character c in h2.Housers)
            {

                foreach (Relationship r in getRelationships(c))
                {
                    // personnage d'autre house
                    Character character = h1.Housers.Find(x => x.Id == r.Character.Id);
                    if (character != null)
                    {
                        if (r.Type == RelationshipEnum.FRIENDSHIP || r.Type == RelationshipEnum.LOVE) score2 -= 50;
                        else score2 += 15;

                    }

                    // personnage du meme house 
                    character = h2.Housers.Find(x => x.Id == r.Character.Id);
                    if (character != null)
                    {
                        if (r.Type == RelationshipEnum.FRIENDSHIP || r.Type == RelationshipEnum.LOVE) score2 += 15;
                        else score2 -= 10;

                    }
                }

            }
            return new int[] { score1, score2 };
        }

        // return -1 if house2 has no housers
        // return -2 if house1 has no housers
        // return 0 if house1 and house2 have no housers
        // return 1 if house1 won
        // return 1 if house2 won
        public int Fight(House h1, House h2, Territory t, War war)
        {
            House winner;
            int owner = 0;
            if (h1.Housers.Count == 0) h1.Housers = getCharacters(h1);
            if (h2.Housers.Count == 0) h1.Housers = getCharacters(h2);

            if (h2.Housers.Count == 0 && h1.Housers.Count > 0) return -1;
            if (h1.Housers.Count == 0 && h2.Housers.Count > 0) return -2;
            if (h1.Housers.Count == 0 && h2.Housers.Count == 0) return 0;
            int score1, score2;
            if (getTerritories(h1).Find(x => x.Id == t.Id) != null)
            {
                owner = 1;
            }
            if (getTerritories(h2).Find(x => x.Id == t.Id) != null)
            {
                owner = 2;
            }
            int[] scores = getScore(h1, h2, t);
            score1 = scores[0];
            score2 = scores[0];
            Random rnd = new Random();

            if (score1 > score2)
            {
                winner = h1;
                float ratio = score2 / score1;
                int damage = (int)Math.Round((rnd.Next(h2.NumberOfUnits/3, h2.NumberOfUnits - h2.NumberOfUnits/5) / ratio));
                h2.NumberOfUnits -= damage;
                int damage1 = (int)Math.Round((rnd.Next(1, h1.NumberOfUnits/2) / ratio));
                h1.NumberOfUnits -= damage1;
                updateHouse(new HouseDTO(h2));
                updateHouse(new HouseDTO(h1));
                if (rnd.Next(1,2) == 2)
                {
                    if (h2.Housers.Count > 0)
                    {
                        int rand = rnd.Next(0, h2.Housers.Count - 1);
                        Character c = h2.Housers[rand];
                        h2.Housers.Remove(c);
                        deleteCharacter(c);
                    }
                }
                if (h2.Housers.Count > 0)
                {
                    int rand = rnd.Next(0, h2.Housers.Count - 1);
                    Character c = h2.Housers[rand];
                    h2.Housers.Remove(c);
                    deleteCharacter(c);
                }
            }
            else if(score1 < score2)
            {
                winner = h2;
                float ratio = score2 / score1;
                int damage = (int)Math.Round((rnd.Next(h1.NumberOfUnits / 3, h1.NumberOfUnits - h1.NumberOfUnits / 5) / ratio));
                h1.NumberOfUnits -= damage;
                int damage1 = (int)Math.Round((rnd.Next(1, h2.NumberOfUnits / 2) / ratio));
                h2.NumberOfUnits -= damage1;
                updateHouse(new HouseDTO(h1));
                updateHouse(new HouseDTO(h2));
                if (rnd.Next(1, 2) == 2)
                {
                    if (h1.Housers.Count > 0)
                    {
                        int rand = rnd.Next(0, h1.Housers.Count - 1);
                        Character c = h1.Housers[rand];
                        h1.Housers.Remove(c);
                        deleteCharacter(c);
                    }
                }
                if (h2.Housers.Count > 0)
                {
                    int rand = rnd.Next(0, h2.Housers.Count - 1);
                    Character c = h2.Housers[rand];
                    h2.Housers.Remove(c);
                    deleteCharacter(c);
                }
            }
            else
            {
                if (owner == 1)
                    winner = h1;
                else if (owner == 2)
                    winner = h2;
                else
                    winner = h2;

                if (h1.Housers.Count > 0)
                {
                    int rand = rnd.Next(0, h1.Housers.Count - 1);
                    Character c = h1.Housers[rand];
                    h1.Housers.Remove(c);
                    deleteCharacter(c);
                }

                if (h2.Housers.Count > 0)
                {
                    int rand = rnd.Next(0, h2.Housers.Count - 1);
                    Character c = h2.Housers[rand];
                    h2.Housers.Remove(c);
                    deleteCharacter(c);
                }

                float ratio = score2 / score1;
                int damage = (int)Math.Round((rnd.Next(1, h1.NumberOfUnits / 2) / ratio));
                h2.NumberOfUnits -= damage;
                int damage1 = (int)Math.Round((rnd.Next(1, h1.NumberOfUnits / 2) / ratio));
                h1.NumberOfUnits -= damage1;
                updateHouse(new HouseDTO(h2));
                updateHouse(new HouseDTO(h1));
            }
            Fight f = new Fight(0, h1, h2, winner, war);
            try
            {
                insertFight(new FightDTO(f));
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while addling fight");
            }
            return winner.Id == h1.Id ? 1 : 2;
        }

        public List<Character> getCharacters(House house)
        {
            DataTable datatable = dalManager.database.getAllCharacters(house);
            List<Character> objs = new List<Character>();

            foreach (DataRow row in datatable.Rows)
            {
                int houseId = -1;
                try
                {
                    houseId = int.Parse(row["HouseId"].ToString());
                }
                catch (Exception e)
                {

                    houseId = -1;
                }

                Character ch = new Character(
                        int.Parse(row["Id"].ToString()),
                        int.Parse(row["Bravoury"].ToString()),
                        int.Parse(row["Crazyness"].ToString()),
                        row["FirstName"].ToString(),
                        row["LastName"].ToString(),
                        int.Parse(row["Pv"].ToString()),
                        (CharacterTypeEnum)int.Parse(row["CharacterType"].ToString()),
                        houseId);
                objs.Add(ch);
            }
            return objs;
        }
        public Character getCharacter(int id)
        {
            DataTable datatable = dalManager.database.getCharacter(id);
            List<Character> objs = new List<Character>();

            foreach (DataRow row in datatable.Rows)
            {
                int houseId = -1;
                try
                {
                    houseId = int.Parse(row["HouseId"].ToString());
                }
                catch (Exception e)
                {

                    houseId = -1;
                }

                Character ch = new Character(
                        int.Parse(row["Id"].ToString()),
                        int.Parse(row["Bravoury"].ToString()),
                        int.Parse(row["Crazyness"].ToString()),
                        row["FirstName"].ToString(),
                        row["LastName"].ToString(),
                        int.Parse(row["Pv"].ToString()),
                        (CharacterTypeEnum)int.Parse(row["CharacterType"].ToString()),
                        houseId);
                objs.Add(ch);
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
        public int insertCharacter(CharacterDTO obj)
        {
            return dalManager.database.insertCharacter(obj);
        }
        public int updateCharacter(CharacterDTO obj)
        {
            return dalManager.database.updateCharacter(obj);
        }
        public int deleteCharacter(Character obj)
        {
            return dalManager.database.deleteCharacter(obj.Id);
        }

        public List<House> getHouses()
        {
            DataTable datatable = dalManager.database.getAllHouses();
            List<House> objs = new List<House>();
            foreach (DataRow row in datatable.Rows)
            {
                House house = new House(int.Parse(row["Id"].ToString()), row["Name"].ToString(), int.Parse(row["NbrOfUnits"].ToString()));
                //house.Housers = getCharacters(house);
                objs.Add(house);

            }
            return objs;
        }
        public House getHouse(int id)
        {
            DataTable datatable = dalManager.database.getHouse(id);
            List<House> objs = new List<House>();

            foreach (DataRow row in datatable.Rows)
            {
                House house = new House(int.Parse(row["Id"].ToString()), row["Name"].ToString(), int.Parse(row["NbrOfUnits"].ToString()));
                //house.Housers = getCharacters(house);
                objs.Add(house);
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
        public int insertHouse(HouseDTO obj)
        {
            return dalManager.database.insertHouse(obj);
        }
        public int updateHouse(HouseDTO obj)
        {
            return dalManager.database.updateHouse(obj);
        }
        public int deleteHouse(House obj)
        {
            return dalManager.database.deleteHouse(obj.Id);
        }

        public List<Territory> getTerritories()
        {
            DataTable datatable = dalManager.database.getAllTerritories();
            List<Territory> objs = new List<Territory>();

            foreach (DataRow row in datatable.Rows)
            {
                int owner = -1;
                try
                {
                    owner = int.Parse(row["Owner"].ToString());
                }
                catch(Exception e)
                {
                    owner = -1;
                }
                House h = getHouse(owner);
                objs.Add(new Territory(int.Parse(row["Id"].ToString()), h, (TerritoryType)int.Parse(row["Type"].ToString())));
            }
            return objs;
        }

        public List<Territory> getTerritories(House h)
        {
            DataTable datatable = dalManager.database.getTerritories(h);
            List<Territory> objs = new List<Territory>();
            foreach (DataRow row in datatable.Rows)
            {
                objs.Add(new Territory(int.Parse(row["Id"].ToString()), h, (TerritoryType)int.Parse(row["Type"].ToString())));
            }
            return objs;
        }
        public Territory getTerritory(int id)
        {
            DataTable datatable = dalManager.database.getTerritory(id);
            List<Territory> objs = new List<Territory>();

            foreach (DataRow row in datatable.Rows)
            {
                int owner = -1;
                try
                {
                    owner = int.Parse(row["Owner"].ToString());
                }
                catch (Exception e)
                {
                    owner = -1;
                }
                House h = getHouse(owner);
                objs.Add(new Territory(int.Parse(row["Id"].ToString()), h, (TerritoryType)int.Parse(row["Type"].ToString())));
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
        public int insertTerritory(TerritoryDTO obj)
        {
            return dalManager.database.insertTerritory(obj);
        }
        public int updateTerritory(TerritoryDTO obj)
        {
            return dalManager.database.updateTerritory(obj);
        }
        public int deleteTerritory(Territory obj)
        {
            return dalManager.database.deleteTerritory(obj.Id);
        }


        public List<Fight> getFights()
        {
            DataTable datatable = dalManager.database.getAllFights();
            List<Fight> objs = new List<Fight>();

            foreach (DataRow row in datatable.Rows)
            {
                House houseChallenging = getHouse(int.Parse(row["HouseChallenging"].ToString()));
                House houseChallenged = getHouse(int.Parse(row["HouseChallenger"].ToString()));
                House houseWinner;
                War war;
                try
                {
                    houseWinner = getHouse(int.Parse(row["HouseWinner"].ToString()));
                }catch(Exception e)
                {
                    houseWinner = null;
                }

                try
                {
                    war = new War(int.Parse(row["WarId"].ToString()));
                }
                catch (Exception e)
                {
                    war = null;
                }
                objs.Add(new Fight(int.Parse(row["Id"].ToString()),houseChallenging, houseChallenged, houseWinner, war));
            }
            return objs;
        }
        public List<Fight> getFights(War war)
        {
            if (war == null) return null;
            DataTable datatable = dalManager.database.getAllFights(war);
            List<Fight> objs = new List<Fight>();

            foreach (DataRow row in datatable.Rows)
            {
                House houseChallenging = getHouse(int.Parse(row["HouseChallenging"].ToString()));
                House houseChallenged = getHouse(int.Parse(row["HouseChallenger"].ToString()));
                House houseWinner;
                try
                {
                    houseWinner = getHouse(int.Parse(row["HouseWinner"].ToString()));
                }
                catch (Exception e)
                {
                    houseWinner = null;
                }
                objs.Add(new Fight(int.Parse(row["Id"].ToString()),houseChallenging, houseChallenged, houseWinner, war));
            }
            return objs;
        }

        public List<Fight> getInitiatedFights(House h)
        {
            if (h == null) return null;
            DataTable datatable = dalManager.database.getInitiatedFights(h);
            List<Fight> objs = new List<Fight>();

            foreach (DataRow row in datatable.Rows)
            {
                House houseChallenging = getHouse(int.Parse(row["HouseChallenging"].ToString()));
                House houseChallenged = getHouse(int.Parse(row["HouseChallenger"].ToString()));
                House houseWinner;
                War war;
                try
                {
                    houseWinner = getHouse(int.Parse(row["HouseWinner"].ToString()));
                }
                catch (Exception e)
                {
                    houseWinner = null;
                }

                try
                {
                    war = new War(int.Parse(row["WarId"].ToString()));
                }
                catch (Exception e)
                {
                    war = null;
                }
                objs.Add(new Fight(int.Parse(row["Id"].ToString()),houseChallenging, houseChallenged, houseWinner, war));
            }
            return objs;
        }

        public List<Fight> getSubmitedFights(House h)
        {
            if (h == null) return null;
            DataTable datatable = dalManager.database.getSubmitedFights(h);
            List<Fight> objs = new List<Fight>();

            foreach (DataRow row in datatable.Rows)
            {
                House houseChallenging = getHouse(int.Parse(row["HouseChallenging"].ToString()));
                House houseChallenged = getHouse(int.Parse(row["HouseChallenger"].ToString()));
                House houseWinner;
                War war;
                try
                {
                    houseWinner = getHouse(int.Parse(row["HouseWinner"].ToString()));
                }
                catch (Exception e)
                {
                    houseWinner = null;
                }

                try
                {
                    war = new War(int.Parse(row["WarId"].ToString()));
                }
                catch (Exception e)
                {
                    war = null;
                }
                objs.Add(new Fight(int.Parse(row["Id"].ToString()),houseChallenging, houseChallenged, houseWinner, war));
            }
            return objs;
        }

        public Fight getFight(int id)
        {
            DataTable datatable = dalManager.database.getFight(id);
            List<Fight> objs = new List<Fight>();

            foreach (DataRow row in datatable.Rows)
            {
                House houseChallenging = getHouse(int.Parse(row["HouseChallenging"].ToString()));
                House houseChallenged = getHouse(int.Parse(row["HouseChallenger"].ToString()));
                House houseWinner;
                War war;
                try
                {
                    houseWinner = getHouse(int.Parse(row["HouseWinner"].ToString()));
                }
                catch (Exception e)
                {
                    houseWinner = null;
                }

                try
                {
                    war = new War(int.Parse(row["WarId"].ToString()));
                }
                catch (Exception e)
                {
                    war = null;
                }
                objs.Add(new Fight(int.Parse(row["Id"].ToString()),houseChallenging, houseChallenged, houseWinner, war));
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
        public int insertFight(FightDTO obj)
        {
            return dalManager.database.insertFight(obj);
        }
        public int updateFight(FightDTO obj)
        {
            return dalManager.database.updateFight(obj);
        }
        public int deleteFight(Fight obj)
        {
            return dalManager.database.deleteFight(obj.Id);
        }

        public List<War> getWars()
        {
            DataTable datatable = dalManager.database.getAllWars();
            List<War> objs = new List<War>();

            foreach (DataRow row in datatable.Rows)
            {
                War war = new War(int.Parse(row["Id"].ToString()));
                war.Fights = getFights(war);
                objs.Add(war);
            }
            return objs;
        }

        public List<War> getWars(House h)
        {
            DataTable datatable = dalManager.database.getWars(h);
            List<War> objs = new List<War>();

            foreach (DataRow row in datatable.Rows)
            {
                War war = new War(int.Parse(row["Id"].ToString()));
                war.Fights = getFights(war);
                objs.Add(war);
            }
            return objs;
        }
        public War getWar(int id)
        {
            DataTable datatable = dalManager.database.getWar(id);
            List<War> objs = new List<War>();

            foreach (DataRow row in datatable.Rows)
            {
                War war = new War(int.Parse(row["Id"].ToString()));
                war.Fights = getFights(war);
                objs.Add(war);
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
        public int insertWar()
        {
            return dalManager.database.insertWar();
        }
        public int deleteWar(War obj)
        {
            return dalManager.database.deleteWar(obj.Id);
        }

        public List<Relationship> getRelationships(Character character)
        {
            DataTable datatable = dalManager.database.getAllRelationships(character);
            List<Relationship> objs = new List<Relationship>();

            foreach (DataRow row in datatable.Rows)
            {
                Character ch = getCharacter(int.Parse(row["SecondCharacherID"].ToString()));
                objs.Add(new Relationship(int.Parse(row["Id"].ToString()), ch,(RelationshipEnum)int.Parse(row["Type"].ToString())));
            }
            return objs;
        }
        public int insertRelationship(Character character, RelationshipDTO obj)
        {
            return dalManager.database.insertRelationship(character,obj);
        }
        public int updateRelationship(Character character, RelationshipDTO obj)
        {
            return dalManager.database.updateRelationship(character,obj);
        }
        public int deleteRelationship(int id)
        {
            return dalManager.database.deleteRelationship(id);
        }
        public Relationship getRelationship(int id)
        {
            DataTable datatable = dalManager.database.getRelationship(id);
            List<Relationship> objs = new List<Relationship>();
            foreach (DataRow row in datatable.Rows)
            {
                Character ch = getCharacter(int.Parse(row["SecondCharacterID"].ToString()));
                objs.Add(new Relationship(int.Parse(row["Id"].ToString()), ch, (RelationshipEnum)int.Parse(row["Type"].ToString())));
            }
            if (objs.Count == 0) return null;
            return objs[0];
        }
    }
}
