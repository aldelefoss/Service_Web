﻿using BuisinessLayer;
using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThronesTournamentConsole
{
    class Program
    {
        static ThronesTournamentManager manager = new ThronesTournamentManager();
        static void Main(string[] args)
        {
            int c = 0;
            while (c != '7') { 
                Console.WriteLine("1 - Houses");
                Console.WriteLine("2 - Characters");
                Console.WriteLine("3 - Fights");
                Console.WriteLine("4 - Wars");
                Console.WriteLine("5 - Territory");
                Console.WriteLine("6 - Reset Data");
                Console.WriteLine("7 - Quit");
                c = Console.Read();

                switch (c)
                {
                    case '1' :
                        while (c != '3') {
                            Console.WriteLine("1 - View all houses");
                            Console.WriteLine("2 - Select House");
                            Console.WriteLine("3 - Add House");
                            Console.WriteLine("4 - Back");
                            Console.ReadLine();
                            c = Console.Read();
                            switch (c){
                                case '1':
                                    viewHouses();
                                    break;

                                case '2':
                                    while (c != '3') { 
                                        Console.WriteLine("1 - By Id");
                                        Console.WriteLine("2 - By Name");
                                        Console.ReadLine();
                                        c = Console.Read();
                                        House house = null;
                                        switch (c)
                                        {
                                            case '1':
                                                Console.ReadLine();
                                                int id = Int32.Parse(Console.ReadLine()); ;
                                                house = selectHouse(id,null);
                                                break;

                                            case '2':
                                                Console.ReadLine();
                                                string name = Console.ReadLine();
                                                house = selectHouse(-1, name);
                                                break;
                                        }
                                        if(house == null) continue;
                                        while (c != '5')
                                        {
                                            Console.WriteLine("===== House ======");
                                            Console.WriteLine("1 - View House Characters");
                                            Console.WriteLine("2 - Add New Character");
                                            Console.WriteLine("3 - Select Character");
                                            Console.WriteLine("4 - Invade Territory");
                                            Console.WriteLine("5 - Back");
                                            Console.ReadLine();
                                            c = Console.Read();

                                            switch (c)
                                            {
                                                case '1':
                                                    viewCharacters(house);
                                                    break;

                                                case '2':
                                                    Console.Write("Enter first name : ");
                                                    Console.ReadLine();
                                                    string fname = Console.ReadLine();
                                                    Console.Write("Enter last name : ");
                                                    Console.ReadLine();
                                                    string lname = Console.ReadLine();
                                                    Console.Write("Enter crazyness: ");
                                                    Console.ReadLine();
                                                    int crazyness = Int32.Parse(Console.ReadLine()); ;
                                                    Console.Write("Enter bravoury: ");
                                                    Console.ReadLine();
                                                    int bravoury = Int32.Parse(Console.ReadLine()); ;
                                                    Console.Write("Enter Life Points : ");
                                                    Console.ReadLine();
                                                    int Points = Int32.Parse(Console.ReadLine()); ;
                                                    Console.Write("Enter Character type  :  1 - .... : ");
                                                    Console.ReadLine();
                                                    int type = Int32.Parse(Console.ReadLine()); ;
                                                    try
                                                    {
                                                        manager.insertCharacter(new CharacterDTO(new Character(0, bravoury, crazyness, fname, lname, Points, (CharacterTypeEnum)type, house.Id)));
                                                    }catch(Exception e)
                                                    {
                                                        Console.WriteLine("Error while adding a character");
                                                    }
                                                    break;
                                                case '3':
                                                    Console.WriteLine("1 - By Id");
                                                    Console.WriteLine("2 - By Name");
                                                    Console.WriteLine("3 - Back");
                                                    Console.ReadLine();
                                                    c = Console.Read();
                                                    Character character = null;
                                                    switch (c)
                                                    {
                                                        case '1':
                                                            Console.ReadLine();
                                                            int id = Int32.Parse(Console.ReadLine()); ;
                                                            character = selectCharacter(id, null);
                                                            break;

                                                        case '2':
                                                            Console.ReadLine();
                                                            string name = Console.ReadLine();
                                                            character = selectCharacter(-1, name);
                                                            break;
                                                    }
                                                    if (character == null) continue;

                                                    while (c!='3')
                                                    {
                                                        Console.WriteLine("1 - Update");
                                                        Console.WriteLine("2 - Delete");
                                                        Console.ReadLine();
                                                        c = Console.Read();

                                                        switch (c)
                                                        {
                                                            case '1':
                                                                Console.WriteLine("What do you want to update");
                                                                Console.ReadLine();
                                                                int cc = Console.Read();
                                                                switch (cc)
                                                                {
                                                                    case '1': 
                                                                        Console.Write("Enter first name : ");
                                                                        Console.ReadLine();
                                                                        string ffname = Console.ReadLine();
                                                                        character.FirstName = ffname;
                                                                        break;
                                                                    case '2':
                                                                        Console.Write("Enter last name : ");
                                                                        Console.ReadLine();
                                                                        string llname = Console.ReadLine();
                                                                        character.LastName = llname;
                                                                    
                                                                        break;
                                                                    case '3':
                                                                        Console.Write("Enter crazyness: "); Console.ReadLine();

                                                                        int ccrazyness = Int32.Parse(Console.ReadLine()); ;
                                                                        character.Crazyness = ccrazyness;


                                                                        break;
                                                                    case '4':
                                                                        Console.Write("Enter bravoury: "); Console.ReadLine();

                                                                        int bbravoury = Int32.Parse(Console.ReadLine()); ;
                                                                        character.Bravoury = bbravoury;


                                                                        break;
                                                                    case '5':
                                                                        Console.Write("Enter Life Points : "); Console.ReadLine();

                                                                        int pPoints = Int32.Parse(Console.ReadLine()); ;
                                                                        character.Pv = pPoints;

                                                                        break;
                                                                    case '6':
                                                                        Console.Write("Enter Character type  :  1 - .... : "); Console.ReadLine();

                                                                        int ttype = Int32.Parse(Console.ReadLine()); ;
                                                                        character.CharacterType = (CharacterTypeEnum)ttype;

                                                                        break;
                                                                }
                                                                try
                                                                {
                                                                    manager.updateCharacter(new CharacterDTO(character));
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    Console.WriteLine("Error deleting charater");
                                                                }
                                                                break;

                                                            case '2':
                                                                try
                                                                {
                                                                    manager.deleteCharacter(character);
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    Console.WriteLine("Error deleting charater");
                                                                }

                                                                break;
                                                        }
                                                    }
                                                    break;
                                                case '4':
                                                    while (c != '3')
                                                    {
                                                        Console.WriteLine("1 - View All Territories");
                                                        Console.WriteLine("2 - Select Terretory");

                                                        c = Console.Read();

                                                        switch (c)
                                                        {
                                                            case '1':
                                                                viewTerritories(null);
                                                                break;

                                                            case '2':
                                                                int id = Int32.Parse(Console.ReadLine()); ;
                                                                Territory territory = selectTerritory(id);
                                                                if (territory == null) continue;
                                                                if (territory.Owner != null)
                                                                {
                                                                    Console.WriteLine("Terretory Is Colonated By House " + territory.Owner.Name);
                                                                    Console.WriteLine("1 - Fight");
                                                                    Console.WriteLine("2 - Back"); 

                                                                    c = Console.Read();
                                                                    if (c == '1') {
                                                                        int fight = manager.Fight(house, territory.Owner, territory, null);
                                                                        if(fight == 1)
                                                                        {
                                                                            territory.Owner = house;
                                                                            try
                                                                            {
                                                                                manager.updateTerritory(new TerritoryDTO(territory));
                                                                            }
                                                                            catch (Exception e)
                                                                            {
                                                                                Console.WriteLine("Error while updating territory");
                                                                            }
                                                                            Console.WriteLine("You have won the fight");
                                                                        }
                                                                        else
                                                                        {
                                                                            Console.WriteLine("You have lost the fight");

                                                                        }
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    territory.Owner = house;
                                                                    try
                                                                    {
                                                                        manager.updateTerritory(new TerritoryDTO(territory));
                                                                    }catch(Exception e)
                                                                    {
                                                                        Console.WriteLine("Error while updating territory");
                                                                    }
                                                                }
                                                                break;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                    case '2' :
                        while (c != '3')
                        {
                            Console.WriteLine("1 - View All Characters");
                            Console.WriteLine("2 - Select A Character");
                            Console.WriteLine("3 - Back"); Console.ReadLine();

                            c = Console.Read();
                            switch (c)
                            {
                                case '1':
                                    viewCharacters(null);
                                    break;
                                case '2':
                                    Console.WriteLine("1 - By Id");
                                    Console.WriteLine("2 - By Name");
                                    Console.WriteLine("3 - Back"); Console.ReadLine();

                                    c = Console.Read();
                                    Character character = null;
                                    switch (c)
                                    {
                                        case '1':
                                            Console.ReadLine();

                                            int id = Int32.Parse(Console.ReadLine());
                                            character = selectCharacter(id, null);
                                            break;

                                        case '2':
                                            Console.ReadLine();

                                            string name = Console.ReadLine();
                                            character = selectCharacter(-1, name);
                                            break;
                                    }
                                    if (character == null) continue;

                                    while (c != '4')
                                    {
                                        Console.WriteLine("2 - Update");
                                        Console.WriteLine("3 - Delete");
                                        Console.WriteLine("4 - Back"); Console.ReadLine();

                                        c = Console.Read();

                                        switch (c)
                                        {
                                            case '1':
                                                Console.WriteLine("1 - View All Relationships");
                                                Console.WriteLine("2 - Add A Relationship");
                                                Console.WriteLine("3 - Delete A Relationship");
                                                Console.WriteLine("4 - Back"); Console.ReadLine();

                                                c = Console.Read();
                                                switch (c)
                                                {
                                                    case '1':
                                                        viewRelationships(character);    
                                                        break;
                                                    case '2':
                                                        Console.WriteLine("Select character => ");
                                                        Console.WriteLine("1 - By Id");
                                                        Console.WriteLine("2 - By Name");
                                                        Console.WriteLine("3 - Back"); Console.ReadLine();


                                                        c = Console.Read();
                                                        Character scharacter = null;
                                                        switch (c)
                                                        {
                                                            case '1':
                                                                Console.ReadLine();

                                                                int i = Int32.Parse(Console.ReadLine()); ;
                                                                scharacter = selectCharacter(i, null);
                                                                break;

                                                            case '2':
                                                                Console.ReadLine();

                                                                string name = Console.ReadLine();
                                                                scharacter = selectCharacter(-1, name);
                                                                break;
                                                        }
                                                        if (scharacter != null)
                                                        {
                                                            Console.WriteLine("Enter Type"); Console.ReadLine();

                                                            int t = Int32.Parse(Console.ReadLine()); ;
                                                            try
                                                            {
                                                                manager.insertRelationship(character, new RelationshipDTO(new Relationship(scharacter, (RelationshipEnum)t)));
                                                            }
                                                            catch (Exception e)
                                                            {
                                                                Console.WriteLine("Error while adding relationship");
                                                            }

                                                        }
                                                        else continue;
                                                        break;
                                                    case '3':
                                                        Console.Write("Enter relationship id : "); Console.ReadLine();

                                                        int id = Int32.Parse(Console.ReadLine());
                                                        Relationship re = selectRelationship(id);
                                                        if(re != null)
                                                        {
                                                            try
                                                            {
                                                                manager.deleteRelationship(re.Id);
                                                            }catch(Exception e)
                                                            {
                                                                Console.WriteLine("Error while deleting relationship");
                                                            }
                                                        }
                                                        break;
                                                    case '4': break;
                                                }
                                                break;
                                            case '2':
                                                Console.WriteLine("What do you want to update"); Console.ReadLine();

                                                int cc = Console.Read();
                                                switch (cc)
                                                {
                                                    case '1':
                                                        Console.Write("Enter first name : "); Console.ReadLine();

                                                        string ffname = Console.ReadLine();
                                                        character.FirstName = ffname;
                                                        break;
                                                    case '2':
                                                        Console.Write("Enter last name : "); Console.ReadLine();

                                                        string llname = Console.ReadLine();
                                                        character.LastName = llname;

                                                        break;
                                                    case '3':
                                                        Console.Write("Enter crazyness: "); Console.ReadLine();

                                                        int ccrazyness = Int32.Parse(Console.ReadLine()); ;
                                                        character.Crazyness = ccrazyness;


                                                        break;
                                                    case '4':
                                                        Console.Write("Enter bravoury: "); Console.ReadLine();

                                                        int bbravoury = Int32.Parse(Console.ReadLine()); ;
                                                        character.Bravoury = bbravoury;


                                                        break;
                                                    case '5':
                                                        Console.Write("Enter Life Points : "); Console.ReadLine();

                                                        int pPoints = Int32.Parse(Console.ReadLine()); ;
                                                        character.Pv = pPoints;

                                                        break;
                                                    case '6':
                                                        Console.Write("Enter Character type  :  1 - .... : "); Console.ReadLine();

                                                        int ttype = Int32.Parse(Console.ReadLine());
                                                        character.CharacterType = (CharacterTypeEnum)ttype;

                                                        break;
                                                }
                                                try
                                                {
                                                    manager.updateCharacter(new CharacterDTO(character));
                                                }
                                                catch (Exception e)
                                                {
                                                    Console.WriteLine("Error deleting charater");
                                                }
                                                break;

                                            case '3':
                                                try
                                                {
                                                    manager.deleteCharacter(character);
                                                }
                                                catch (Exception e)
                                                {
                                                    Console.WriteLine("Error deleting charater");
                                                }

                                                break;
                                        }
                                    }
                                    break;


                            }
                        }
                        break;
                    case '3':
                        while (c != '4')
                        {
                            Console.WriteLine("1 - View All Fights");
                            Console.WriteLine("2 - Start A Fight");
                            Console.WriteLine("4 - Back"); Console.ReadLine();

                            c = Console.Read();
                            switch (c)
                            {
                                case '1':
                                    viewFights();
                                    break;
                                case '2':
                                    Console.WriteLine("Select first house =>");
                                    Console.WriteLine("1 - By Id");
                                    Console.WriteLine("2 - By Name"); Console.ReadLine();
                                    c = Console.Read();
                                    House house = null;
                                    switch (c)
                                    {
                                        case '1':
                                            Console.ReadLine();
                                            int id = Int32.Parse(Console.ReadLine());
                                            Console.WriteLine(id);
                                            house = selectHouse(id, null);
                                            break;

                                        case '2':
                                            string name = Console.ReadLine();
                                            house = selectHouse(-1, name);
                                            break;
                                    }
                                    if (house == null) continue;

                                    Console.WriteLine("Select second house =>");
                                    Console.WriteLine("1 - By Id");
                                    Console.WriteLine("2 - By Name"); Console.ReadLine();

                                    c = Console.Read();
                                    House house2 = null;
                                    switch (c)
                                    {
                                        case '1':
                                            Console.ReadLine();
                                            int id2 = Int32.Parse(Console.ReadLine()); ;
                                            house2 = selectHouse(id2, null);
                                            break;

                                        case '2':
                                            Console.ReadLine();
                                            string name = Console.ReadLine();
                                            house2 = selectHouse(-1, name);
                                            break;
                                    }
                                    if (house2 == null) continue;

                                    Console.WriteLine("Enter Territory id : "); Console.ReadLine();

                                    int id3 = Int32.Parse(Console.ReadLine()); ;
                                    Territory t = selectTerritory(id3);
                                    if (t == null) continue;

                                    int fight = manager.Fight(house, house2, t, null);
                                    if (fight == 1)
                                    {
                                        t.Owner = house;
                                        Console.WriteLine("You have won the fight");
                                        
                                    }
                                    else
                                    {
                                        t.Owner = house2;
                                        Console.WriteLine("You have lost the fight");

                                    }
                                    try
                                    {
                                        manager.updateTerritory(new TerritoryDTO(t));
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("Error while updating territory");
                                    }

                                    break;
                                case '3': break;

                            }
                        }
                        break;
                    case '4':
                        while (c != '2')
                        {
                            Console.WriteLine("1 - View All Wars ");
                            Console.WriteLine("2 - Back"); Console.ReadLine();

                            c = Int32.Parse(Console.ReadLine());
                            switch (c)
                            {
                                case '1':
                                    viewWars();
                                    break;
                            }
                        }
                        break;
                    case '5':
                        while (c != '3')
                        {
                            Console.WriteLine("1 - View All Territories ");
                            Console.WriteLine("2 - Add Territory");
                            Console.WriteLine("3 - Back"); Console.ReadLine();

                            c = Int32.Parse(Console.ReadLine());
                            switch (c)
                            {
                                case '1':
                                    viewWars();
                                    break;
                                case '2':
                                    break;
                            }
                        }
                        break;
                    case '6':
                        break;

                }
                Console.ReadLine();
            }
        }

        static House selectHouse(int id, string name)
        {
            House h;
            if (id == -1)
            {
                List<House> houses = manager.getHouses();
                h = houses.Find(x => x.Name == name);
            }
            else
            {
                h = manager.getHouse(id);
            }
            if (h == null)
            {
                if(id != -1 )
                Console.WriteLine("House "+id+" Not Found  !!!");
                else Console.WriteLine("House " + name + " Not Found  !!!");

            }
            else
            {
                Console.WriteLine("Id \t Name \t\t Units \t Housers \t");
                Console.WriteLine("-----------------------------------------------------");
                Console.WriteLine(h.Id + "\t" + h.Name + "\t\t" + h.NumberOfUnits + "\t" + manager.getCharacters(h).Count);
            }
            return h;
        }

        static War selectWar(int id)
        {
            War War = manager.getWar(id);

            if (War == null)
            {
                Console.WriteLine("War Not Found !!!");
            }
            else
            {
                Console.WriteLine("#########");
                Console.WriteLine("War #" + War.Id);
                Console.WriteLine("#########");

                List<Fight> Fights = manager.getFights(War);
                if (Fights.Count == 0)
                {
                    Console.WriteLine("No Fights Available");
                }
                else
                {
                    Console.WriteLine("WarId \t Id \t Challenger \t Challenged \t Winner");
                    Console.WriteLine("-----------------------------------------------------");

                }
                foreach (Fight Fight in Fights)
                {
                    Console.WriteLine(War.Id + "\t" + Fight.Id + "\t" + Fight.houseChallenging.Name + "\t" + Fight.houseChallenged.Name + "\t" + Fight.houseWinner.Name);
                }
            }
            return War;
        }
        static Territory selectTerritory(int id)
        {
            Territory Territory  = manager.getTerritory(id);
            
            if (Territory == null)
            {
                Console.WriteLine("Territory Not Found !!!");
            }
            else
            {
                Console.WriteLine("Id \t Owner \t\t Type");
                Console.WriteLine("-----------------------------------------------------");
                Console.WriteLine(Territory.Id + "\t" + Territory.Owner.Name + "\t" + Territory.type);

            }   
            return Territory;
        }

        static Character selectCharacter(int id, string name)
        {
            Character Character;
            if (id == -1)
            {
                List<Character> Characters = manager.getCharacters();
                Character = Characters.Find(x => x.FirstName + x.LastName == name);
            }
            else
            {
                Character = manager.getCharacter(id);
            }
            if (Character == null)
            {
                Console.WriteLine("Character Not Found !!!");
            }
            else
            {
                Console.WriteLine("Id \t First Name \t\t Last Name \t\t Life \t Bravoury \t Craziness \t Type");
                Console.WriteLine("-----------------------------------------------------");
                Console.WriteLine(
                                    Character.Id + "\t" +
                                    Character.FirstName + "\t\t" +
                                    Character.LastName + "\t\t" +
                                    Character.Pv + "\t" +
                                    Character.Bravoury + "\t" +
                                    Character.Crazyness + "\t" +
                                    Character.CharacterType + "\t");
            }
            return Character;
        }

        static Fight selectFight(int id)
        {
            Fight Fight = manager.getFight(id);
            if (Fight == null)
            {
                Console.WriteLine("Fight Not Found !!!");
            }
            else
            {
                Console.WriteLine("Id \t Challenger \t Challenged \t Winner");
                Console.WriteLine("-----------------------------------------------------");
                string n1 = "DELETED\t\t";
                string n2 = "DELETED\t";
                string n3 = "DELETED\t";
                if (Fight.houseChallenging != null) n1 = Fight.houseChallenging.Name;
                if (Fight.houseChallenged != null) n2 = Fight.houseChallenged.Name;
                if (Fight.houseWinner != null) n3 = Fight.houseWinner.Name;
                Console.WriteLine(Fight.Id + "\t" + n1 + "\t" + n2 + "\t" + n3);
            }
            return Fight;
        }

        static Relationship selectRelationship(int id)
        {
            Relationship Relationship = manager.getRelationship(id);

            

            if (Relationship == null)
            {
                Console.WriteLine("Relationship Not Found ");
            }
            else
            {
                Console.WriteLine("Id \t Character \t\t\t\t Relationship Type");
                Console.WriteLine("-----------------------------------------------------");
                Console.WriteLine(Relationship.Id + "\t" + Relationship.Character.FirstName + " " + Relationship.Character.LastName + "\t" + Relationship.Type);
            }
            return Relationship;

        }

        static void viewHouses()
        {
            List<House> houses = manager.getHouses();
            if (houses.Count == 0)
            {
                Console.WriteLine("No Houses Available");
            }
            else
            {
                Console.WriteLine("Id \t Name \t\t Units \t Housers \t");
                Console.WriteLine("-----------------------------------------------------");

            }
            foreach (House house in houses)
            {
                Console.WriteLine(house.Id +"\t"+house.Name + "\t\t" + house.NumberOfUnits + "\t" + manager.getCharacters(house).Count);
            }
        }

        static void viewFights()
        {
            List<Fight> Fights = manager.getFights();
            if (Fights.Count == 0)
            {
                Console.WriteLine("No Fights Available");
            }
            else
            {
                Console.WriteLine("Id \t Challenger \t Challenged \t Winner");
                Console.WriteLine("-----------------------------------------------------");

            }
            foreach (Fight Fight in Fights)
            {
                string n1 = "DELETED";
                string n2 = "DELETED";
                string n3 = "DELETED";
                if (Fight.houseChallenging != null)  n1=Fight.houseChallenging.Name;
                if (Fight.houseChallenged != null) n2 = Fight.houseChallenged.Name;
                if (Fight.houseWinner != null) n3 = Fight.houseWinner.Name;
                Console.WriteLine(Fight.Id +"\t"+ n1 + "\t" + n2 + "\t" + n3);
            }
        }

        static void viewWars()
        {
            List<War> Wars = manager.getWars();
            if (Wars.Count == 0)
            {
                Console.WriteLine("No Wars Available");
            }
            foreach (War War in Wars)
            {
                Console.WriteLine("#########");
                Console.WriteLine("War #" + War.Id);
                Console.WriteLine("#########");

                List<Fight> Fights = manager.getFights(War);
                if (Fights.Count == 0)
                {
                    Console.WriteLine("No Fights Available");
                }
                else
                {
                    Console.WriteLine("WarId \t Id \t Challenger \t Challenged \t Winner");
                    Console.WriteLine("-----------------------------------------------------");

                }
                foreach (Fight Fight in Fights)
                {
                    Console.WriteLine(War.Id + "\t" + Fight.Id + "\t" + Fight.houseChallenging.Name + "\t" + Fight.houseChallenged.Name + "\t" + Fight.houseWinner.Name);
                }
            }
        }

        static void viewCharacters(House h)
        {
            List<Character> Characters;
            if (h == null)
                Characters = manager.getCharacters();
            else
                Characters = manager.getCharacters(h);

            if (Characters.Count == 0)
            {
                Console.WriteLine("No Characters Available");
            }
            else
            {
                Console.WriteLine("Id \t First Name \t\t Last Name \t\t Life \t Bravoury \t Craziness \t Type");
                Console.WriteLine("-----------------------------------------------------");

            }
            foreach (Character Character in Characters)
            {
                Console.WriteLine(
                    Character.Id + "\t" +
                    Character.FirstName + "\t\t" +
                    Character.LastName + "\t\t" +
                    Character.Pv + "\t" +
                    Character.Bravoury + "\t" +
                    Character.Crazyness + "\t" +
                    Character.CharacterType + "\t");
            }
        }

        static void viewRelationships(Character c)
        {
            List<Relationship> Relationships;
            if (c == null)
                return;
            else
                Relationships = manager.getRelationships(c);

            if (Relationships.Count == 0)
            {
                Console.WriteLine("No Relationships Available");
            }
            else
            {
                Console.WriteLine("Id \t Character \t\t\t\t Relationship Type");
                Console.WriteLine("-----------------------------------------------------");

            }
            foreach (Relationship Relationship in Relationships)
            {
                Console.WriteLine(Relationship.Id + "\t" + Relationship.Character.FirstName + " " + Relationship.Character.LastName + "\t" + Relationship.Type);
            }
        }


        static void viewTerritories(House house)
        {
            List<Territory> Territories;
            if (house ==null)
                Territories = manager.getTerritories();
            else
                Territories = manager.getTerritories();

            if (Territories.Count == 0)
            {
                Console.WriteLine("Territory Not Found !!!");
            }
            else
            {
                foreach(Territory Territory in Territories)
                {
                    Console.WriteLine("Id \t Owner \t\t Type");
                    Console.WriteLine("-----------------------------------------------------");
                    if(Territory.Owner != null)
                        Console.WriteLine(Territory.Id + "\t" + Territory.Owner.Name + "\t" + Territory.type);
                    else
                        Console.WriteLine(Territory.Id + "\t (: NO ONE :) \t" + Territory.type);

                }
            }
        }


    }


}
