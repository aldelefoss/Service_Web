﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;

namespace StubDataAccessLayer
{
    public class DalManager
    {
        private static DalManager dalManager;
        List<House> listHouse;
        List<Character> listCharacter;
        List<Territory> listTerritory;

        public static DalManager getInstance()
        {
            if (dalManager == null) dalManager = new DalManager();
            return dalManager;
        }
        private DalManager()
        {
            listCharacter = new List<Character>();
            listHouse = new List<House>();
            listTerritory = new List<Territory>();


            /*  Character Theon = new Character(10, 100, "Theon", "Greyjoy", 25, CharacterTypeEnum.LOSER);
              Character Yara = new Character(10, 100, "Yara", "Greyjoy", 25, CharacterTypeEnum.WARRIOR);
              Character Daenerys = new Character(100, 100, "Daenerys", "Targaryen", 95, CharacterTypeEnum.LEADER);
              Character Arya = new Character(90, 80, "Arya", "Stark", 100, CharacterTypeEnum.WARRIOR);
              Character Jaime = new Character(50, 70, "Jaime", "Lanniseter", 95, CharacterTypeEnum.WARRIOR);
              Character Tyrion = new Character(50, 20, "Tyrion", "Lanniseter", 50, CharacterTypeEnum.TACTICIAN);
              Character Jon = new Character(100, 100, "Jon", "Targaryen", 80, CharacterTypeEnum.WARRIOR);
              Character Bran = new Character(100, 100, "Bran", "Stark", 80, CharacterTypeEnum.WARRIOR);
              Character Cersei = new Character(100, 80, "Cersei", "Lanniseter", 80, CharacterTypeEnum.LEADER);

              listCharacter.Add(Theon);
              listCharacter.Add(Daenerys);
              listCharacter.Add(Arya);
              listCharacter.Add(Jaime);
              listCharacter.Add(Tyrion);
              listCharacter.Add(Jon);

              House houseGreyjoy = new House("Greyjoy", 150);
              houseGreyjoy.addHousers(Theon);
              houseGreyjoy.addHousers(Yara);
              listHouse.Add(houseGreyjoy);

              House houseTargaryen = new House("Targaryen", 100);
              houseTargaryen.addHousers(Daenerys);
              houseTargaryen.addHousers(Jon);
              listHouse.Add(houseTargaryen);


              House houseStarks = new House("Starks", 250);
              houseStarks.addHousers(Arya);
              houseStarks.addHousers(Bran);
              listHouse.Add(houseStarks);

              House houseLannister = new House("Lanniseters", 500);
              houseLannister.addHousers(Tyrion);
              houseLannister.addHousers(Jaime);
              houseLannister.addHousers(Cersei);
              listHouse.Add(houseLannister);

              Territory territory = new Territory(houseLannister, TerritoryType.LAND);
              listTerritory.Add(territory);
              territory = new Territory(houseStarks, TerritoryType.MOUNTAIN);
              listTerritory.Add(territory);
              territory = new Territory(houseGreyjoy, TerritoryType.SEA);
              listTerritory.Add(territory);
              territory = new Territory(houseTargaryen, TerritoryType.DESERT);
              listTerritory.Add(territory);*/


        }
        public List<House> getExistingHouses()
        {
            return listHouse;
        }

        public List<House> getBigHouses()
        {
            List<House> list = new List<House>();
            foreach (House h in listHouse)
            {
                if (h.NumberOfUnits > 200)
                    list.Add(h);
            }

            return list;
        }

        public List<Territory> getExistingTerrytory()
        {
            return listTerritory;
        }

        public List<Character> getExistingCharacters()
        {
            return listCharacter;
        }
    }
}
