﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntitiesLayer;
using RestAPI.Models;

namespace EntitiesLayerTests
{
    [TestClass]
    public class WarTest
    {
        BuisinessLayer.ThronesTournamentManager manager = new BuisinessLayer.ThronesTournamentManager();
        [TestMethod]
        public void TestAjoutWar()
        {
            int nbWars = manager.getWars().Count;
            manager.insertWar();
            Assert.AreNotEqual(nbWars, manager.getWars().Count);
        }
       
        [TestMethod]
        public void TestDeleteWar()
        {
            War c = getWar();
            manager.deleteWar(c);
            Assert.AreEqual(manager.getWar(c.Id), null);
        }

        [TestMethod]
        public void TestGetWarFights()
        {
            War c = getWar();
            int count = manager.getFights(c).Count;
            insertFight();
            int count2 = manager.getFights(c).Count;
            Assert.AreNotEqual(count2, count);
        }


        void insertFight()
        {
            HouseDTO h1 = new HouseDTO(new House("Dupont", 500));
            manager.insertHouse(h1);
            HouseDTO h2 = new HouseDTO(new House("Dupond", 480));
            manager.insertHouse(h2);
            manager.insertWar();
            int nbFights = manager.getFights().Count;
            FightDTO c = new FightDTO(new Fight(nbFights, manager.getHouses()[0], manager.getHouses()[1], null, manager.getWars()[0]));
            manager.insertFight(c);
        }
        public War getWar()
        {
            War c = manager.getWars()[0];
            if (c == null)
            {
                manager.insertWar();
                c = manager.getWars()[0];
            }
            return c;
        }

    }
}
