﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntitiesLayer;
using RestAPI.Models;

namespace EntitiesLayerTests
{
    [TestClass]
    public class CharacterTest
    {
        BuisinessLayer.ThronesTournamentManager manager = new BuisinessLayer.ThronesTournamentManager();
        [TestMethod]
        public void TestAjoutCharacter()
        {
            int nbCharacters = manager.getCharacters().Count;
            CharacterDTO c = new CharacterDTO(new Character(0, 100, 50, "Tyrion", "Lannister", 50, 0, getHouse().Id));
            manager.insertCharacter(c);
            Assert.AreNotEqual(nbCharacters, manager.getCharacters().Count);
        }
        [TestMethod]
        public void TestUpdateCharacter()
        {
            Character c = getCharacter();
            CharacterDTO cdto = new CharacterDTO(c);
            cdto.Pv = 0;
            manager.updateCharacter(cdto);
            Assert.AreEqual(cdto.Pv, manager.getCharacter(c.Id).Pv);
        }
        [TestMethod]
        public void TestDeleteCharacter()
        {
            Character c = getCharacter();
            manager.deleteCharacter(c);
            Assert.AreEqual(manager.getCharacter(c.Id), null);
        }
        [TestMethod]
        public void TestAddRelationship()
        {
            Character c = getCharacter();
            int nbRelationships = manager.getRelationships(c).Count;
            manager.insertRelationship(c, new RelationshipDTO(new Relationship(c, 0)));
            Assert.AreNotEqual(manager.getRelationships(c).Count, nbRelationships);
        }
        [TestMethod]
        public void TestDeleteRelationship()
        {
            Character c = getCharacter();

            int nbRelationships = manager.getRelationships(c).Count;
            manager.deleteRelationship(getRelationship().Id);
            Assert.AreNotEqual(manager.getRelationships(c).Count, nbRelationships);
        }
        public Character getCharacter()
        {
            Character c = manager.getCharacters()[0];
            if (c == null)
            {
                CharacterDTO cdto = new CharacterDTO(new Character(0, 100, 50, "Tyrion", "Lannister", 50, 0, 1));
                manager.insertCharacter(cdto);
                c = manager.getCharacters()[0];
            }
            return c;
        }
        public Relationship getRelationship()
        {
            Relationship r = manager.getRelationships(getCharacter())[0];
            if (r == null)
            {
                manager.insertRelationship(getCharacter(), new RelationshipDTO(new Relationship(getCharacter(), 0)));
                r = manager.getRelationships(getCharacter())[0];
            }
            return r;
        }

        public House getHouse()
        {
            House c = manager.getHouses()[0];
            if (c == null)
            {
                HouseDTO cdto = new HouseDTO(new House("ZZ", 50));
                manager.insertHouse(cdto);
                c = manager.getHouses()[0];
            }
            return c;
        }
    }
}
