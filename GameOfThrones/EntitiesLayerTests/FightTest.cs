﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntitiesLayer;
using RestAPI.Models;

namespace EntitiesLayerTests
{
    [TestClass]
    public class FightTest
    {
        BuisinessLayer.ThronesTournamentManager manager = new BuisinessLayer.ThronesTournamentManager();
        [TestMethod]
        public void TestAjoutFight()
        {
            HouseDTO h1 = new HouseDTO(new House("Dupont", 500));
            manager.insertHouse(h1);
            HouseDTO h2 = new HouseDTO(new House("Dupond", 480));
            manager.insertHouse(h2);
            manager.insertWar();
            int nbFights = manager.getFights().Count;
            FightDTO c = new FightDTO(new Fight(nbFights, manager.getHouses()[0], manager.getHouses()[1], null, manager.getWars()[0]));
            manager.insertFight(c);
            Assert.AreNotEqual(nbFights, manager.getFights().Count);
        }
        [TestMethod]
        public void TestScore()
        {
            HouseDTO h1 = new HouseDTO(new House("Dupont", 500));
            manager.insertHouse(h1);
            HouseDTO h2 = new HouseDTO(new House("Dupond", 480));
            manager.insertHouse(h2);
            TerritoryDTO t = new TerritoryDTO(new Territory(0, manager.getHouses()[0], TerritoryType.LAND));
            manager.insertTerritory(t);

            House house1 = manager.getHouses()[0];
            House house2 = manager.getHouses()[1];
            CharacterDTO cdto = new CharacterDTO(new Character(0, 100, 50, "Tyrion", "Lannister", 50, 0, house1.Id));
            manager.insertCharacter(cdto);

            cdto = new CharacterDTO(new Character(0, 100, 50, "zz", "zz", 50, 0, house2.Id));
            manager.insertCharacter(cdto);


            house1.Housers = manager.getCharacters(house1);
            house2.Housers = manager.getCharacters(house2);

            Territory territory = manager.getTerritories(house1)[0];
            int[] scores = manager.getScore(house1, house2, territory);
            int countHousers1 = house1.Housers.Count;
            int countHousers2 = house2.Housers.Count;
            manager.Fight(house1, house2, territory, null);
            if(scores[0] > scores[1])
            {
                Assert.AreNotEqual(countHousers2, manager.getHouses()[2].Housers.Count);
            }else if(scores[0] < scores[1])
            {
                Assert.AreNotEqual(countHousers1, manager.getHouses()[1].Housers.Count);
            }
            else if(scores[0] == scores[1])
            {
                Assert.AreNotEqual(countHousers1, manager.getHouses()[1].Housers.Count);
                Assert.AreNotEqual(countHousers2, manager.getHouses()[2].Housers.Count);
            }
            else{
                Assert.AreEqual(countHousers1, manager.getHouses()[1].Housers.Count);
                Assert.AreEqual(countHousers2, manager.getHouses()[2].Housers.Count);
            }
        }
        [TestMethod]
        public void TestUpdateFight()
        {
            Fight c = getFight();
            FightDTO cdto = new FightDTO(c);
            cdto.houseWinner = cdto.houseChallenged;
            manager.updateFight(cdto);
            Assert.AreEqual(cdto.houseWinner.Id, manager.getFight(c.Id).houseWinner.Id);
        }
        [TestMethod]
        public void TestDeleteFight()
        {
            Fight c = getFight();
            manager.deleteFight(c);
            Assert.AreEqual(manager.getFight(c.Id), null);
        }
        public Fight getFight()
        {
            Fight c = manager.getFights()[0];
            if (c == null)
            {
                HouseDTO h1 = new HouseDTO(new House("Dupont", 500));
                manager.insertHouse(h1);
                HouseDTO h2 = new HouseDTO(new House("Dupond", 480));
                manager.insertHouse(h2);
                manager.insertWar();
                int nbFights = manager.getFights().Count;
                FightDTO cdto = new FightDTO(new Fight(nbFights, manager.getHouses()[0], manager.getHouses()[1], null, manager.getWars()[0]));
                manager.insertFight(cdto);
                c = manager.getFights()[0];
            }
            return c;
        }
    }
}
