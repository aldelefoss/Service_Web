﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntitiesLayer;
using RestAPI.Models;

namespace EntitiesLayerTests
{
    [TestClass]
    public class TerritoryTest
    {
        BuisinessLayer.ThronesTournamentManager manager = new BuisinessLayer.ThronesTournamentManager();
        [TestMethod]
        public void TestAjoutTerritory()
        {
            int nbTerritorys = manager.getTerritories().Count;
            TerritoryDTO c = new TerritoryDTO(new Territory(0,getHouse(), TerritoryType.SEA));
            manager.insertTerritory(c);
            Assert.AreNotEqual(nbTerritorys, manager.getTerritories().Count);
        }
        [TestMethod]
        public void TestUpdateTerritory()
        {
            Territory c = getTerritory();
            TerritoryDTO cdto = new TerritoryDTO(c);
            cdto.type = (TerritoryType.MOUNTAIN).ToString();
            manager.updateTerritory(cdto);
            Assert.AreEqual(TerritoryType.MOUNTAIN, manager.getTerritory(c.Id).type);
        }
        [TestMethod]
        public void TestDeleteTerritory()
        {
            Territory c = getTerritory();
            manager.deleteTerritory(c);
            Assert.AreEqual(manager.getTerritory(c.Id), null);
        }

        public House getHouse()
        {
            House c = manager.getHouses()[0];
            if (c == null)
            {
                HouseDTO cdto = new HouseDTO(new House("ZZ", 50));
                manager.insertHouse(cdto);
                c = manager.getHouses()[0];
            }
            return c;
        }

        public Territory getTerritory()
        {
            Territory c = manager.getTerritories()[0];
            if (c == null)
            {
                TerritoryDTO cdto = new TerritoryDTO(new Territory(0, getHouse(), TerritoryType.SEA));
                manager.insertTerritory(cdto);
                c = manager.getTerritories()[0];
            }
            return c;
        }
    }
}
