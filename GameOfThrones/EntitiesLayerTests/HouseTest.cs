﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntitiesLayer;
using RestAPI.Models;

namespace EntitiesLayerTests
{
    [TestClass]
    public class HouseTest
    {
        BuisinessLayer.ThronesTournamentManager manager = new BuisinessLayer.ThronesTournamentManager();
        [TestMethod]
        public void TestAjoutHouse()
        {
            int nbHouses = manager.getHouses().Count;
            HouseDTO c = new HouseDTO(new House("ZZ", 50));
            manager.insertHouse(c);
            Assert.AreNotEqual(nbHouses, manager.getHouses().Count);
        }
        [TestMethod]
        public void TestUpdateHouse()
        {
            House c = getHouse();
            HouseDTO cdto = new HouseDTO(c);
            cdto.NumberOfUnits = 0;
            manager.updateHouse(cdto);
            Assert.AreEqual(cdto.NumberOfUnits, manager.getHouse(c.Id).NumberOfUnits);
        }
        [TestMethod]
        public void TestDeleteHouse()
        {
            House c = getHouse();
            manager.deleteHouse(c);
            Assert.AreEqual(manager.getHouse(c.Id), null);
        }
        public House getHouse()
        {
            House c = manager.getHouses()[0];
            if (c == null)
            {
                HouseDTO cdto = new HouseDTO(new House("ZZ", 50));
                manager.insertHouse(cdto);
                c = manager.getHouses()[0];
            }
            return c;
        }
    }
}
