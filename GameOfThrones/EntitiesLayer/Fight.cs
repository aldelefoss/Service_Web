﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Fight : EntityObject
    {
        public House houseChallenging { get; set; }
        public House houseChallenged { get; set; }
        public House houseWinner { get; set; }
        public War war { get; set; }

        public Fight(int id, House Challenging, House Challenged, House Winner, War war)
        {
            houseChallenged = Challenged;
            houseChallenging= Challenging;
            houseWinner = Winner;
            this.war = war;
            Id = id;
            
        }

    }
}
