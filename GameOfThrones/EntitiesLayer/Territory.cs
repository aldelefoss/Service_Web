﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Territory : EntityObject
    {
        public TerritoryType type { get; set; }
        public House Owner { get; set; }
        public Territory(int id, House owner, TerritoryType type)
        {
            this.type = type;
            Owner = owner;
            Id = id;
        }
        public string toString()
        {
            return "Owner : " + Owner.Name + " -> " + type.ToString();
        }
    }

    public enum TerritoryType
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
}
