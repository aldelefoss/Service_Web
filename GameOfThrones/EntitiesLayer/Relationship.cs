﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Relationship : EntityObject
    {
        public Character Character { get; set; }
        public RelationshipEnum Type { get; set; }
        public Relationship(int id, Character character, RelationshipEnum type)
        {
            Id = id;
            this.Character = character;
            this.Type = type;
        }

        public Relationship(Character character, RelationshipEnum type)
        {
            Id = -1;
            this.Character = character;
            this.Type = type;
        }

    }
    public enum RelationshipEnum
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }
}
