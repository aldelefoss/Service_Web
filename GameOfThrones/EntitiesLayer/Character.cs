﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Character : EntityObject
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Pv{ get; set; }

        public List<Relationship> Relationships { get; set; }

        public CharacterTypeEnum CharacterType { get; set; }

        public House house { get; set; }

        public Character(int id, int bravoury, int crazyness, string firstName, string lastName, int pv, CharacterTypeEnum characterType, int houseId)
        {
            house = new House();
            Id = id; 
            this.Bravoury= bravoury;
            this.Crazyness = crazyness;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Pv = pv;
            this.house.Id = houseId;
            CharacterType = characterType;
            Relationships = new List<Relationship>();
        }


        public void addRelatives(Character character, RelationshipEnum type)
        {
            Relationships.Add(new Relationship(character, type));
        }

        public string toString()
        {
            return "Character : " + FirstName + " " + LastName + ", Bravory " + Bravoury + ", crazyness " + Crazyness + ", Lifepoints " + Pv+ "\n";
        }

    }
    public enum CharacterTypeEnum
    {
        WARRIOR,
        WITCH,
        TACTICIAN,
        LEADER,
        LOSER
    }
}
