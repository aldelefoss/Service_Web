﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class TerritoryForHouseDTO : EntityObject
    {
        public string type { get; set; }
        public TerritoryForHouseDTO(Territory t)
        {
            this.type = ((TerritoryType)t.type).ToString();
            Id = t.Id;
        }
        public string toString()
        {
            return  type.ToString();
        }
    }
}