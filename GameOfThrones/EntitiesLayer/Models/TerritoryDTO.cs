﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class TerritoryDTO : EntityObject
    {
        public string type { get; set; }
        public HouseDTO Owner { get; set; }
        public TerritoryDTO(Territory t)
        {
            this.type = ((TerritoryType)t.type).ToString();
            Owner = (t.Owner == null)? null :  new HouseDTO(t.Owner);
            Id = t.Id;
        }
        public string toString()
        {
            return "Owner : " + Owner.Name + " -> " + type.ToString();
        }
    }
}