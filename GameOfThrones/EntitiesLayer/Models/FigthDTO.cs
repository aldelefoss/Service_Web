﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class FightDTO : EntityObject
    {
        public HouseDTO houseChallenging { get; set; }
        public HouseDTO houseChallenged { get; set; }
        public HouseDTO houseWinner { get; set; }
        public int warId { get; set; }


        public FightDTO(Fight fight)
        {
            houseChallenging = new HouseDTO(fight.houseChallenging);
            houseChallenged = new HouseDTO(fight.houseChallenged);
            if (fight.houseWinner != null)
                houseWinner = new HouseDTO(fight.houseWinner);
            else
                houseWinner = null;
            Id = fight.Id;
            if(fight.war != null)
            {
                warId = fight.war.Id;
            }
            else
            {
                warId = -1;
            }
            
        }

    }
}