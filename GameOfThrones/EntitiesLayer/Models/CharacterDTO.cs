﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class CharacterDTO : EntityObject
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Pv { get; set; }


        public string CharacterType { get; set; }

        public int house { get; set; }


        public CharacterDTO(Character character)
        {
            this.Bravoury = character.Bravoury;
            this.Crazyness = character.Crazyness;
            this.FirstName = character.FirstName;
            this.LastName = character.LastName;
            this.Pv = character.Pv;
            this.house = character.house.Id;
            this.Id = character.Id;
            CharacterType = ((CharacterTypeEnum)character.CharacterType).ToString();

        }

        public CharacterDTO()
        {

        }

        public string toString()
        {
            return "Character : " + FirstName + " " + LastName + ", Bravory " + Bravoury + ", crazyness " + Crazyness + ", Lifepoints " + Pv + "\n";
        }

    }

}
