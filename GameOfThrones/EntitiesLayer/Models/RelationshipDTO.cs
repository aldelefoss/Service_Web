﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class RelationshipDTO : EntityObject
    {
        public CharacterDTO Character { get; set; }
        public string Type { get; set; }
        public RelationshipDTO(Relationship relationship)
        {
            this.Character = new CharacterDTO(relationship.Character);
            this.Type = ((RelationshipEnum)relationship.Type).ToString();
            this.Id = relationship.Id;
        }
        public RelationshipDTO()
        {

        }

    }

}