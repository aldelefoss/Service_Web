﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class WarDTO : EntityObject
    {
        public List<FightDTO> Fights { get; set; }
        public WarDTO(War w)
        {
            Fights = new List<FightDTO>();
            foreach (Fight f in w.Fights) 
                Fights.Add(new FightDTO(f));
            Id = w.Id;
        }

        public WarDTO(List<FightDTO> fights)
        {
            Fights = fights;
        }
    }
}