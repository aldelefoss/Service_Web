﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestAPI.Models
{
    public class HouseDTO : EntityObject
    {
        public string Name { get; set; }
        public int NumberOfUnits { get; set; }
       // public List<CharacterDTO> characters { get; set; }

        public HouseDTO(House house)
        {
            if (house == null) return;
            Id = house.Id;
            Name = house.Name;
            NumberOfUnits = house.NumberOfUnits;
            //characters = new List<CharacterDTO>();
            /*foreach (Character c in house.Housers)
            {
                characters.Add(new CharacterDTO(c));
            }*/
        }
        public HouseDTO()
        {
            //characters = new List<CharacterDTO>();
        }

        public void addHousers(CharacterDTO cha)
        {
           // characters.Add(cha);
        }

    }
}