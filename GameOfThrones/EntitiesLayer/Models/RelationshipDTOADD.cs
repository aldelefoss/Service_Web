﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestAPI.Models
{
    public class RelationshipDTOADD
    {
        public int FirstCharacterID { get; set; }
        public int SecondCharacterID { get; set; }
        public string RelationshipType { get; set; }

        public RelationshipDTOADD()
        {

        }
    }
}
