﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class War : EntityObject
    {
        public List<Fight> Fights { get; set; }
        public War()
        {
            Fights = new List<Fight>();
        }

        public War(int id)
        {
            Id = id;
        }

        public War(int id, List<Fight> fights)
        {
            Id = id;
            Fights = fights;
        }
    }
}
