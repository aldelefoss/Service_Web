﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class House : EntityObject
    {
        public List<Character> Housers { get; set; }
        public string Name { get; set; }
        public int NumberOfUnits { get; set; }
        public void addHousers(Character character)
        {
            Housers.Add(character);
        }
        public House()
        {

        }

        public House(string name, int nbr)
        {
            Name = name;
            NumberOfUnits = nbr;
            Housers = new List<Character>();
        }

        public House(int id, string name, int nbr)
        {
            Id = id;
            Name = name;
            NumberOfUnits = nbr;
            Housers = new List<Character>();
        }

        public string toString()
        {
            string result = "Name : " +Name+", Number of uinits :" + NumberOfUnits + ", Hoursers [ \n" ;
            foreach (Character c in Housers)
            {
                result += c.toString();
            }
            result += "]";
            return result;
        }
    }
}
