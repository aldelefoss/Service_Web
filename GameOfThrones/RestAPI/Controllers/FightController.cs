﻿using BuisinessLayer;
using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestAPI.Controllers
{
    [RoutePrefix("api/fight")]
    public class FightController : ApiController
    {
        ThronesTournamentManager manager = new ThronesTournamentManager();
        public IHttpActionResult getFights()
        {
            List<FightDTO> fights = new List<FightDTO>();
            foreach (Fight fight in manager.getFights())
            {
                fights.Add(new FightDTO(fight));
            }
            return Json(fights);
        }

        [Route("{id}")]
        public IHttpActionResult getFight(int id)
        {
            if (manager.getFight(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new FightDTO(manager.getFight(id)));
        }
        [Route("{id}/war")]
        public IHttpActionResult getWar(int id)
        {
            Fight w = manager.getFight(id);
            if (w == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            if (w.war == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new WarDTO(manager.getWar(w.war.Id)));
        }

        [HttpDelete]
        [Route("{id}")]
        public void deleteFight(int id)
        {
            Fight c = manager.getFight(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            try
            {
                manager.deleteFight(c);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }

        [HttpPost]
        public void createFight([FromBody] FightDTO p)
        {
            try
            {
                manager.insertFight(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public void updateFight(int id, [FromBody] FightDTO p)
        {
            try
            {
                p.Id = id;
                manager.updateFight(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
