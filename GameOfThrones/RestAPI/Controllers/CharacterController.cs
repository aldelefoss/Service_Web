﻿using BuisinessLayer;
using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestAPI.Controllers
{
    [RoutePrefix("api/character")]
    public class CharacterController : ApiController
    {
        ThronesTournamentManager manager = new ThronesTournamentManager();
        public IHttpActionResult getCharacters()
        {
            List<CharacterDTO> houses = new List<CharacterDTO>();
            foreach (Character house in manager.getCharacters())
            {
                houses.Add(new CharacterDTO(house));
            }
            return Json(houses);
        }
        [Route("{id}")]
        public IHttpActionResult getCharacter(int id)
        {
            if (manager.getCharacter(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new CharacterDTO(manager.getCharacter(id)));
        }
        [Route("{id}/relationships")]
        public IHttpActionResult getRelationships(int id)
        {
            List<RelationshipDTO> objs = new List<RelationshipDTO>();
            Character c = manager.getCharacter(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            foreach (Relationship relation in manager.getRelationships(c))
            {
                objs.Add(new RelationshipDTO(relation));
            }
            return Json(objs);
        }

        [Route("{id}/house")]
        public IHttpActionResult getHouse(int id)
        {
            Character c = manager.getCharacter(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }

            if (c.house.Id == -1)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }

            return Json(new HouseDTO(manager.getHouse(c.house.Id)));
        }

        [HttpDelete]
        [Route("{id}")]
        public void deleteCharacter(int id)
        {
            Character c = manager.getCharacter(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            try
            {
                manager.deleteCharacter(c);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }

        [HttpPost]
        public void createCharacter([FromBody] CharacterDTO p)
        {
            try
            {
                manager.insertCharacter(p);
            }catch(Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public void updateCharacter(int id,[FromBody] CharacterDTO p)
        {
            try
            {
                p.Id = id;
                manager.updateCharacter(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [Route("{id}/addRelationship")]
        public void addRelationship(int id, [FromBody] RelationshipDTOADD re)
        {
            Character c = manager.getCharacter(re.FirstCharacterID);
            if(c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            Character c2 = manager.getCharacter(re.SecondCharacterID);
            if (c2 == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            RelationshipDTO r = new RelationshipDTO();
            r.Character = new CharacterDTO(c2);
            r.Type = re.RelationshipType;
            manager.insertRelationship(c, r);
        }


        [HttpPost]
        [Route("{id}/removeRelationship/{relationshipId}")]
        public void removeRelationship(int id, int relationshipId)
        {
            Boolean found = false ;
            Character c = manager.getCharacter(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            List<Relationship> rs = manager.getRelationships(c);
            foreach(Relationship r in rs)
            {
                if (r.Id == relationshipId) found = true;
            }
            if (!found)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            manager.deleteRelationship(relationshipId);
        }



    }
}
