﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestAPI.Models;
using BuisinessLayer;
using EntitiesLayer;

namespace RestAPI.Controllers
{
    [RoutePrefix("api/territory")]
    public class TerritoryController : ApiController
    {
        ThronesTournamentManager manager = new ThronesTournamentManager();

        public IHttpActionResult getTerritories()
        {
            List<TerritoryDTO> territories = new List<TerritoryDTO>();
            foreach (Territory t in manager.getTerritories())
            {
                territories.Add(new TerritoryDTO(t));
            }
            return Json(territories);
        }

        [Route("{id}")]
        public IHttpActionResult getTerritory(int id)
        {
            Territory t = manager.getTerritory(id);
            if (t == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new TerritoryDTO(t));
        }

        [HttpDelete]
        [Route("{id}")]
        public void deleteTerritory(int id)
        {
            Territory c = manager.getTerritory(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            try
            {
                manager.deleteTerritory(c);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }
        [HttpPost]
        public void createTerritory([FromBody] TerritoryDTO p)
        {
            try
            {
                manager.insertTerritory(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public void updateTerritory(int id, [FromBody] TerritoryDTO p)
        {
            try
            {
                p.Id = id;
                manager.updateTerritory(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
