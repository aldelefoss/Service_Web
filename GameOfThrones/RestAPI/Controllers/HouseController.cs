﻿using BuisinessLayer;
using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;



namespace RestAPI.Controllers
{
    [RoutePrefix("api/house")]
    public class HouseController : ApiController
    {
        ThronesTournamentManager manager = new ThronesTournamentManager();
        public IHttpActionResult getHouses()
        {
            List<HouseDTO> houses = new List<HouseDTO>();
            foreach (House house in manager.getHouses())
            {
                houses.Add(new HouseDTO(house));
            }
            return Json(houses);
        }

        [Route("{id}")]
        public IHttpActionResult getHouse(int id)
        {
            if (manager.getHouse(id) == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new HouseDTO(manager.getHouse(id)));
        }
        [Route("{id}/characters")]
        public IHttpActionResult getHousers(int id)
        {
            House h = manager.getHouse(id);
            if (h == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            List<CharacterDTO> objs = new List<CharacterDTO>();
            foreach (Character c in manager.getCharacters(h))
            {
                objs.Add(new CharacterDTO(c));
            }
            return Json(objs);
        }

        [Route("{id}/fights")]
        public IHttpActionResult getFights(int id)
        {
            House h = manager.getHouse(id);
            if (h == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            List<FightDTO> initObjs = new List<FightDTO>();
            foreach (Fight c in manager.getInitiatedFights(h))
            {
                initObjs.Add(new FightDTO(c));
            }

            List<FightDTO> submitObjs = new List<FightDTO>();
            foreach (Fight c in manager.getSubmitedFights(h))
            {
                submitObjs.Add(new FightDTO(c));
            }

            return Json(new { Submited = submitObjs, Initiated = initObjs });
        }

        [Route("{id}/territories")]
        public IHttpActionResult getTerritories(int id)
        {
            House h = manager.getHouse(id);
            if (h == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            List<TerritoryForHouseDTO> objs = new List<TerritoryForHouseDTO>();
            foreach (Territory c in manager.getTerritories(h))
            {
                objs.Add(new TerritoryForHouseDTO(c));
            }

            return Json(objs);
        }

        [Route("{id}/wars")]
        public IHttpActionResult getWars(int id)
        {
            House h = manager.getHouse(id);
            if (h == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            List<WarDTO> objs = new List<WarDTO>();
            foreach (War c in manager.getWars(h))
            {
                objs.Add(new WarDTO(c));
            }

            return Json(objs);
        }

        [HttpDelete]
        [Route("{id}")]
        public void deleteHouse(int id)
        {
            House c = manager.getHouse(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            try
            {
                manager.deleteHouse(c);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }

        [HttpPost]
        public void createHouse([FromBody] HouseDTO p)
        {
            try
            {
                manager.insertHouse(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public void updateHouse(int id, [FromBody] HouseDTO p)
        {
            try
            {
                p.Id = id;
                manager.updateHouse(p);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

    }

}
