﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EntitiesLayer;
using BuisinessLayer;
using RestAPI.Models;
namespace RestAPI.Controllers
{
    [RoutePrefix("api/war")]
    public class WarController : ApiController
    {
        ThronesTournamentManager manager = new ThronesTournamentManager();

        public IHttpActionResult getWars()
        {
            List<WarDTO> territories = new List<WarDTO>();
            foreach (War t in manager.getWars())
            {
                territories.Add(new WarDTO(t));
            }
            return Json(territories);
        }

        [Route("{id}")]
        public IHttpActionResult getWar(int id)
        {
            War t = manager.getWar(id);
            if (t == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);

            }
            return Json(new WarDTO(t));
       }

        [HttpDelete]
        [Route("{id}")]
        public void deleteWar(int id)
        {
            War c = manager.getWar(id);
            if (c == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            try
            {
                manager.deleteWar(c);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }

        [HttpPost]
        public void createWar()
        {
            try
            {
                manager.insertWar();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

    }
}
