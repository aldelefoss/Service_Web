﻿CREATE TABLE [dbo].[Relationship] (
    [Id]                INT      IDENTITY (1, 1) NOT NULL,
    [FirstCharacterID]  INT      NOT NULL,
    [SecondCharacterID] INT      NOT NULL,
    [Type]              SMALLINT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED ([SecondCharacterID] ASC, [FirstCharacterID] ASC),
    CONSTRAINT [FK_Relationship_FirstCharacter] FOREIGN KEY ([FirstCharacterID]) REFERENCES [dbo].[Character] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Relationship_SecondCharacter] FOREIGN KEY ([SecondCharacterID]) REFERENCES [dbo].[Character] ([Id]) ON DELETE CASCADE
);

