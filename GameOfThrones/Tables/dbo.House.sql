﻿CREATE TABLE [dbo].[House] (
    [Id]         INT        IDENTITY (1, 1) NOT NULL,
    [Name]       NCHAR (20) NULL,
    [NbrOfUnits] INT        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

