﻿CREATE TABLE [dbo].[Fight] (
    [Id]               INT IDENTITY (1, 1) NOT NULL,
    [HouseChallenging] INT NOT NULL,
    [HouseChallenger]  INT NOT NULL,
    [HouseWinner]      INT NULL,
    [WarId]            INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Fight_HouseChallenging] FOREIGN KEY ([HouseChallenging]) REFERENCES [dbo].[House] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Fight_HouseChallenger] FOREIGN KEY ([HouseChallenger]) REFERENCES [dbo].[House] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Fight_HouseWinner] FOREIGN KEY ([HouseWinner]) REFERENCES [dbo].[House] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Fight_ToTable] FOREIGN KEY ([WarId]) REFERENCES [dbo].[War] ([Id]) ON DELETE CASCADE
);

