﻿CREATE TABLE [dbo].[Character] (
    [Id]            INT        IDENTITY (1, 1) NOT NULL,
    [Bravoury]      SMALLINT   NULL,
    [Crazyness]     SMALLINT   NULL,
    [FirstName]     NCHAR (30) NULL,
    [LastName]      NCHAR (10) NULL,
    [Pv]            SMALLINT   NULL,
    [CharacterType] SMALLINT   NULL,
    [HouseId]       INT        NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Character_ToTable] FOREIGN KEY ([HouseId]) REFERENCES [dbo].[House] ([Id]) ON DELETE SET NULL
);

