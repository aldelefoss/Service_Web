﻿CREATE TABLE Character
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Bravoury] SMALLINT NULL, 
    [Crazyness] SMALLINT NULL, 
    [FirstName] NCHAR(30) NULL, 
    [LastName] NCHAR(10) NULL, 
    [Pv] SMALLINT NULL, 
    [CharacterType] SMALLINT NULL, 
    [HouseId] INT NULL, 
    CONSTRAINT [FK_Character_ToTable] FOREIGN KEY (HouseId) REFERENCES House(Id)
)
