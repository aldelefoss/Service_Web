﻿CREATE TABLE Territory
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Type] NCHAR(10) NULL, 
    [Owner] INT NULL, 
    CONSTRAINT [FK_Territory_ToTable] FOREIGN KEY (Owner) REFERENCES House(Id)
)
