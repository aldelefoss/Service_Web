﻿CREATE TABLE [dbo].[Fight] (
    [Id]               INT IDENTITY (1, 1) NOT NULL,
    [HouseChallenging] INT NULL,
    [HouseChallenger]  INT NULL,
    [HouseWinner]      INT NULL,
    [WarId]            INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Fight_ToTable] FOREIGN KEY ([WarId]) REFERENCES [dbo].[War] ([Id]) ON DELETE CASCADE
);

