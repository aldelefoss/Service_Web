﻿CREATE TABLE [dbo].[Relationship] (
    [Id]                INT      IDENTITY (1, 1) NOT NULL,
    [FirstCharacterID]  INT      NULL,
    [SecondCharacterID] INT      NULL,
    [Type]              SMALLINT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_Relationship_FirstCharacter] FOREIGN KEY ([FirstCharacterID]) REFERENCES [dbo].[Character] ([Id])  ON DELETE SET NULL,
    CONSTRAINT [FK_Relationship_SecondCharacter] FOREIGN KEY ([SecondCharacterID]) REFERENCES [dbo].[Character] ([Id])  ON DELETE SET NULL
);

