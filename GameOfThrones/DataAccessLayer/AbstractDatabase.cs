﻿using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public interface AbstractDatabase
    {
        DataTable getAllCharacters();
        DataTable getAllCharacters(House house);
        DataTable getCharacter(int id);
        int insertCharacter(CharacterDTO obj);
        int deleteCharacter(int id);
        int updateCharacter(CharacterDTO obj);

        DataTable getAllHouses();
        DataTable getHouse(int id);
        int insertHouse(HouseDTO obj);
        int deleteHouse(int id);
        int updateHouse(HouseDTO obj);


        DataTable getAllTerritories();
        DataTable getTerritory(int id);
        DataTable getTerritories(House house);
        int insertTerritory(TerritoryDTO obj);
        int deleteTerritory(int id);
        int updateTerritory(TerritoryDTO obj);

        DataTable getAllWars();
        DataTable getWars(House h);
        DataTable getWar(int id);
        int insertWar();
        int deleteWar(int id);

        DataTable getAllRelationships();
        DataTable getAllRelationships(Character character);
        DataTable getRelationship(int id);
        int insertRelationship(Character c, RelationshipDTO obj);
        int deleteRelationship(int id);
        int updateRelationship(Character c, RelationshipDTO obj);

        DataTable getAllFights();
        DataTable getAllFights(War war);
        DataTable getSubmitedFights(House h);
        DataTable getInitiatedFights(House h);
        DataTable getFight(int id);
        int insertFight(FightDTO obj);
        int deleteFight(int id);
        int updateFight(FightDTO obj);


    }
}
