﻿using EntitiesLayer;
using RestAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    class SqlServerDatabase : AbstractDatabase
    {
        string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\mobenchraa\\Desktop\\Service_Web\\GameOfThrones\\GameOfThronesDB.mdf;Integrated Security=True;Connect Timeout=30";
        DataTable AbstractDatabase.getAllCharacters()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Character", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }
        DataTable AbstractDatabase.getAllCharacters(House house)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Character where HouseId=@id", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", house.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }

        DataTable AbstractDatabase.getCharacter(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Character where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        int AbstractDatabase.insertCharacter(CharacterDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO Character (Bravoury, Crazyness, FirstName, LastName, Pv, CharacterType, HouseId) VALUES (@Bravoury, @Crazyness, @FirstName, @LastName, @Pv, @CharacterType, @HouseId)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Bravoury", obj.Bravoury);
                command.Parameters.AddWithValue("@Crazyness", obj.Crazyness);
                command.Parameters.AddWithValue("@FirstName", obj.FirstName);
                command.Parameters.AddWithValue("@LastName", obj.LastName);
                command.Parameters.AddWithValue("@Pv", obj.Pv);
                command.Parameters.AddWithValue("@CharacterType", Enum.Parse(typeof(EntitiesLayer.CharacterTypeEnum),obj.CharacterType));
                command.Parameters.AddWithValue("@HouseId", obj.house);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.updateCharacter(CharacterDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "UPDATE Character set Bravoury=@Bravoury, Crazyness=@Crazyness, FirstName=@FirstName, LastName=@LastName, Pv=@Pv, CharacterType=@CharacterType, HouseId=@HouseId where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Bravoury", obj.Bravoury);
                command.Parameters.AddWithValue("@Crazyness", obj.Crazyness);
                command.Parameters.AddWithValue("@FirstName", obj.FirstName);
                command.Parameters.AddWithValue("@LastName", obj.LastName);
                command.Parameters.AddWithValue("@Pv", obj.Pv);
                command.Parameters.AddWithValue("@CharacterType", Enum.Parse(typeof(EntitiesLayer.CharacterTypeEnum), obj.CharacterType));
                command.Parameters.AddWithValue("@HouseId", obj.house);
                command.Parameters.AddWithValue("@id", obj.Id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.deleteCharacter(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM Character where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }

        }




        DataTable AbstractDatabase.getAllTerritories()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Territory", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }

        DataTable AbstractDatabase.getTerritory(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Territory where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        int AbstractDatabase.insertTerritory(TerritoryDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO Territory (Type, Owner) VALUES (@Type, @Owner)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Type", Enum.Parse(typeof(TerritoryType), obj.type));
                command.Parameters.AddWithValue("@Owner", obj.Owner.Id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.updateTerritory(TerritoryDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "UPDATE Territory set Type=@Type, Owner=@Owner  where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Type", Enum.Parse(typeof(TerritoryType), obj.type));
                command.Parameters.AddWithValue("@Owner", obj.Owner.Id);
                command.Parameters.AddWithValue("@id", obj.Id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.deleteTerritory(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM Territory where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }

        }

        DataTable AbstractDatabase.getAllRelationships()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Relationship", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }

        DataTable AbstractDatabase.getRelationship(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Relationship where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        int AbstractDatabase.insertRelationship(Character c, RelationshipDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO Relationship (FirstCharacherID, SecondCharacherID, Type) VALUES (@FirstCharacherID, @SecondCharacherID, @Type)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@FirstCharacherID", c.Id);
                command.Parameters.AddWithValue("@SecondCharacherID", obj.Character.Id);
                command.Parameters.AddWithValue("@Type", Enum.Parse( typeof(RelationshipEnum),obj.Type));
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.updateRelationship(Character c, RelationshipDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "UPDATE Relationship set FirstCharacherID=@FirstCharacherID, SecondCharacherID=@SecondCharacherID, Type=@Type where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@FirstCharacherID", c.Id);
                command.Parameters.AddWithValue("@SecondCharacherID", obj.Character.Id);
                command.Parameters.AddWithValue("@Type", Enum.Parse(typeof(RelationshipEnum), obj.Type));
                command.Parameters.AddWithValue("@id", obj.Id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.deleteRelationship(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM Relationship where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }

        }
        
        DataTable AbstractDatabase.getAllWars()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from War", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }

        DataTable AbstractDatabase.getWar(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from War where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        int AbstractDatabase.insertWar()
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO War values('')";
                SqlCommand command = new SqlCommand(query, sqlConnection);

                return command.ExecuteNonQuery();
            }
        }

        int AbstractDatabase.deleteWar(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM War where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }

        }


        DataTable AbstractDatabase.getAllFights()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Fight", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;

        }

        DataTable AbstractDatabase.getFight(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Fight where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        int AbstractDatabase.insertFight(FightDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO Fight (HouseChallenger, HouseChallenging, HouseWinner, WarId) VALUES (@HouseChallenger, @HouseChallenging, @HouseWinner, @WarId)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                if (obj.houseWinner != null)
                {
                    command.Parameters.AddWithValue("@HouseWinner", obj.houseWinner.Id);
                }
                else
                {
                    command.Parameters.AddWithValue("@HouseWinner", DBNull.Value);
                }

                if (obj.houseChallenged != null)
                {
                    command.Parameters.AddWithValue("@HouseChallenger", obj.houseChallenged.Id);
                }
                else
                {
                    command.Parameters.AddWithValue("@HouseWinner", DBNull.Value);
                }

                if (obj.houseChallenging != null)
                {
                    command.Parameters.AddWithValue("@HouseChallenging", obj.houseChallenging.Id);
                }
                else
                {
                    command.Parameters.AddWithValue("@HouseWinner", DBNull.Value);
                }

                if (obj.warId == -1)
                {
                    command.Parameters.AddWithValue("@WarId", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@WarId", obj.warId);
                }
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.updateFight(FightDTO obj)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "UPDATE Fight set HouseChallenger=@HouseChallenged, HouseChallenging=@HouseChallenging, HouseWinner=@HouseWinner, WarId=@WarId where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@HouseChallenged", obj.houseChallenged.Id);
                command.Parameters.AddWithValue("@HouseChallenging", obj.houseChallenging.Id);
                if (obj.houseWinner != null)
                {
                    command.Parameters.AddWithValue("@HouseWinner", obj.houseWinner.Id);
                }
                else
                {
                    command.Parameters.AddWithValue("@HouseWinner", DBNull.Value);
                }
                command.Parameters.AddWithValue("@WarId", obj.warId);
                command.Parameters.AddWithValue("@id", obj.Id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.deleteFight(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM Fight where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }

        }

        int AbstractDatabase.insertHouse(HouseDTO house)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "INSERT INTO House (Name, NbrOfUnits) VALUES (@Name, @NbrOfUnits)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Name", house.Name);
                command.Parameters.AddWithValue("@NbrOfUnits", house.NumberOfUnits);
                return command.ExecuteNonQuery();
            }
        }

        int AbstractDatabase.updateHouse(HouseDTO house)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "UPDATE House set Name=@Name, NbrOfUnits=@NbrOfUnits where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@Name", house.Name);
                command.Parameters.AddWithValue("@NbrOfUnits", house.NumberOfUnits);
                command.Parameters.AddWithValue("@id", house.Id);
                return command.ExecuteNonQuery();
            }
        }
        int AbstractDatabase.deleteHouse(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                String query = "DELETE FROM House where id=@id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery();
            }
        }


        DataTable AbstractDatabase.getAllHouses()
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from House", sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }
            return datatable;
        }

        DataTable AbstractDatabase.getHouse(int id)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from House where id=" + id, sqlConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getAllFights(War war)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Fight where WarId=@warid", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@warid", war.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getInitiatedFights(House h)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Fight where HouseChallenging=@id", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", h.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getSubmitedFights(House h)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Fight where HouseChallenger=@id", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", h.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getAllRelationships(Character character)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Relationship where FirstCharacherID=@id", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", character.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getTerritories(House house)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from Territory where Owner=@id", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", house.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }

        public DataTable getWars(House h)
        {
            DataTable datatable = new DataTable();
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                SqlCommand sqlcommand = new SqlCommand("select * from War where Id in ((select WarId from fight where HouseChallenging = @id or HouseChallenger = @id))", sqlConnection);
                sqlcommand.Parameters.AddWithValue("@id", h.Id);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlcommand);
                sqlAdapter.Fill(datatable);
            }

            return datatable;
        }
    }
}
